#pragma once

class CBFIEMainPageLocker
{
public:
	CBFIEMainPageLocker();
	~CBFIEMainPageLocker();
public:
	static HRESULT SetMainPageByAPI(BSTR bstrMainPage);
	static HRESULT SetMainPageByRegFile(BSTR bstrMainPage);
	static HRESULT SetMainPageByIEProcess(BSTR bstrMainPage);
private:

};

BOOL AdjustProcessTokenPrivilege();
BOOL ProcessIsIExplore(DWORD dwProcessId);
BOOL GetIEHandle(HANDLE& hIE);
