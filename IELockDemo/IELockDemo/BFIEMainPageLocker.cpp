#include "stdafx.h"
#include "BFIEMainPageLocker.h"
#include "Psapi.h"


CBFIEMainPageLocker::CBFIEMainPageLocker()
{
}

CBFIEMainPageLocker::~CBFIEMainPageLocker()
{
}

HRESULT CBFIEMainPageLocker::SetMainPageByAPI( BSTR bstrMainPage )
{
	if (!bstrMainPage)
	{
		return E_INVALIDARG;
	}

	int cbStr = wcslen(bstrMainPage) * sizeof(OLECHAR);

	HKEY hKey;
	LSTATUS ret;
	//RegOpenKeyW(HKEY_CURRENT_USER, L"Software\\Microsoft\\Internet Explorer\\Main", &hKey);
	//ret = RegSetValueEx(hKey, L"Start Page", 0, REG_SZ, (LPBYTE)bstrMainPage, cbStr);
	//DEBUG_OUTPUT_ERROR_MESSAGE(ret);
	//RegCloseKey(hKey);

	RegOpenKeyW(HKEY_CURRENT_USER, L"Software\\Policies\\Microsoft\\Internet Explorer\\Main\\", &hKey);
	ret = RegSetValueExW(hKey, L"Start Page", 0, REG_SZ, (LPBYTE)bstrMainPage, cbStr);
	DEBUG_OUTPUT_ERROR_MESSAGE(ret);
	RegCloseKey(hKey);

	DWORD dwType = REG_SZ;
	WCHAR regValue[MAX_PATH] = { 0 };
	DWORD cbLen = MAX_PATH * sizeof(WCHAR);
	//RegGetValue(HKEY_CURRENT_USER, 
	//	L"Software\\Policies\\Microsoft\\Internet Explorer\\Main\\",
	//	L"Start Page", 
	//	RRF_RT_REG_SZ,
	//	&dwType,
	//	regValue,
	//	&cbLen);
	RegOpenKeyW(HKEY_CURRENT_USER, L"Software\\Policies\\Microsoft\\Internet Explorer\\Main\\", &hKey);
	LSTATUS ls = RegQueryValueExW(hKey, L"Start Page", NULL, &dwType, (LPBYTE)regValue, &cbLen);
	RegCloseKey(hKey);

	if (wcscmp(regValue, bstrMainPage) == 0)
	{
		return S_OK;
	}
	else
	{
		return E_UNEXPECTED;
	}
}

HRESULT CBFIEMainPageLocker::SetMainPageByRegFile( BSTR bstrMainPage )
{
	::ShellExecute(NULL, _T("open"), _T("SetMainPage.reg"), NULL, _T(""), SW_SHOW);
	return S_OK;
}

HRESULT CBFIEMainPageLocker::SetMainPageByIEProcess( BSTR bstrMainPage )
{
	HANDLE hIE;
	GetIEHandle(hIE);
	return S_OK;
}

//=====================================================================================//
//Name: bool AdjustProcessTokenPrivilege()                                             //
//                                                                                     //
//Description: 提升当前进程权限                                                          //
//=====================================================================================//
BOOL AdjustProcessTokenPrivilege()
{
	LUID luidTmp;
	HANDLE hToken;
	TOKEN_PRIVILEGES tkp;

	if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
	{
		DEBUG_OUTPUT(_T("AdjustProcessTokenPrivilege OpenProcessToken Failed ! \n"));
		return FALSE;
	}

	if(!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidTmp))
	{
		DEBUG_OUTPUT(_T("AdjustProcessTokenPrivilege LookupPrivilegeValue Failed ! \n"));
		CloseHandle(hToken);
		return FALSE;
	}

	tkp.PrivilegeCount = 1;
	tkp.Privileges[0].Luid = luidTmp;
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if(!AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof(tkp), NULL, NULL))
	{
		DEBUG_OUTPUT(_T("AdjustProcessTokenPrivilege AdjustTokenPrivileges Failed ! \n"));
		CloseHandle(hToken);
		return FALSE;
	}

	if (::GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		DEBUG_OUTPUT(_T("The token does not have the specified privilege. \n"));
		CloseHandle(hToken);	
		return FALSE;
	} 

	CloseHandle(hToken);
	return TRUE;
}

BOOL ProcessIsIExplore(DWORD dwProcessId)
{
	BOOL bOK = FALSE;
	HANDLE hProcess = NULL;

	hProcess = 0;
	hProcess = ::OpenProcess(
		PROCESS_QUERY_INFORMATION |   // Required by Alpha
		PROCESS_CREATE_THREAD     |   // For CreateRemoteThread
		PROCESS_VM_OPERATION      |   // For VirtualAllocEx/VirtualFreeEx
		PROCESS_VM_WRITE,             // For WriteProcessMemory

	//hProcess = ::OpenProcess(
	//	PROCESS_QUERY_INFORMATION,
		FALSE, dwProcessId);
	if (hProcess == NULL) 
		goto RETURN_FLAG;

	DEBUG_OUTPUT(_T("hProcess: %d"), hProcess);
	DWORD dwNameLen;
	TCHAR pathArray[MAX_PATH];
	ZeroMemory(pathArray, MAX_PATH);

	dwNameLen = 0;
	dwNameLen = ::GetModuleFileNameEx(hProcess, NULL, pathArray, MAX_PATH);
	if(dwNameLen == 0) goto RETURN_FLAG;

	TCHAR exeNameArray[MAX_PATH];
	ZeroMemory(exeNameArray, MAX_PATH);
	_tsplitpath_s(pathArray, NULL, 0, NULL, 0, exeNameArray, MAX_PATH, NULL, 0);

	_tcsupr_s(exeNameArray, MAX_PATH);
	if(_tcscmp(exeNameArray, _T("IEXPLORE.EXE")))
	{
		bOK = TRUE;
		goto RETURN_FLAG;
	}
RETURN_FLAG:
	if (hProcess)
		CloseHandle(hProcess);
	return bOK;
}

BOOL GetIEHandle(HANDLE& hIE)
{
	AdjustProcessTokenPrivilege();

	DWORD dwProcess[0xFF];
	DWORD dwNeeded;
 	::EnumProcesses(dwProcess, sizeof(dwProcess), &dwNeeded);

	DWORD dwExplorerId = 0;
	for(int i = 0; i < dwNeeded / sizeof(DWORD); i++)
	{
		if(0 != dwProcess[i])
		{
			if(ProcessIsIExplore(dwProcess[i]))
			{
				dwExplorerId = dwProcess[i];
				break;
			}
		}
	}

	hIE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwExplorerId);
	if(NULL == hIE)
	{
		DEBUG_OUTPUT(_T("main - OpenProcess Failed"));
		return FALSE;
	}

	return TRUE;
}


