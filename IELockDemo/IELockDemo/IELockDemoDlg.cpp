
// IELockDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IELockDemo.h"
#include "IELockDemoDlg.h"
#include "afxdialogex.h"
#include <atlwin.h>
#include "BFIEMainPageLocker.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CIELockDemoDlg dialog



CIELockDemoDlg::CIELockDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CIELockDemoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIELockDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CIELockDemoDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(IDC_SetMainPage, OnSetMainPage)
END_MESSAGE_MAP()


// CIELockDemoDlg message handlers

BOOL CIELockDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CWnd* editURL = GetDlgItem(IDC_EDIT_MainPageURL);
	editURL->SetWindowTextW(L"http://www.baofeng.com/");

	int aMouseInfo[3];    // Array for mouse information

	// Get the current mouse speed.         
	BOOL fResult = ::SystemParametersInfo(SPI_GETMOUSE,   // Get mouse information
		0,              // Not used
		&aMouseInfo,    // Holds mouse information
		0);             // Not used        

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIELockDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIELockDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CIELockDemoDlg::OnSetMainPage()
{
	CWnd* editURL = GetDlgItem(IDC_EDIT_MainPageURL);
	
	CString sURL;
	editURL->GetWindowTextW(sURL);
	if (sURL.IsEmpty())
	{
		return;
	}
	CComBSTR bstrURL = sURL;
	
	OSVERSIONINFO osvi;
	BOOL bIsWindows7;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	GetVersionEx(&osvi);

	bIsWindows7 = (osvi.dwMajorVersion == 6);

	if (bIsWindows7)
	{
		HRESULT setOK = CBFIEMainPageLocker::SetMainPageByAPI(bstrURL);
		if (SUCCEEDED(setOK))
		{
			::MessageBox(this->GetSafeHwnd(), _T("IE home page set succeeded."), _T("IE Home Page"), MB_ICONINFORMATION);
		}
		else
		{
			::MessageBox(this->GetSafeHwnd(), _T("IE home page set failed."), _T("IE Home Page"), MB_ICONERROR);
		}
	}
	else
	{
		::MessageBox(this->GetSafeHwnd(), _T("Current OS is not Windows 7"), _T("IE Home Page"), MB_ICONWARNING);
	}
	

}

