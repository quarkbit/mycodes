#include <Windows.h>
#include <windowsx.h>
#include <assert.h>
#include <tchar.h>

#include "..\InjectionLib\InjectionLib.h"

//=====================================================================================//
//Name: bool AdjustProcessTokenPrivilege()                                             //
//                                                                                     //
//Description: 提升当前进程权限                                                          //
//=====================================================================================//
BOOL AdjustProcessTokenPrivilege()
{
	LUID luidTmp;
	HANDLE hToken;
	TOKEN_PRIVILEGES tkp;

	if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
	{
		OutputDebugString(_T("AdjustProcessTokenPrivilege OpenProcessToken Failed ! \n"));
		return FALSE;
	}

	if(!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidTmp))
	{
		OutputDebugString(_T("AdjustProcessTokenPrivilege LookupPrivilegeValue Failed ! \n"));
		CloseHandle(hToken);
		return FALSE;
	}

	tkp.PrivilegeCount = 1;
	tkp.Privileges[0].Luid = luidTmp;
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if(!AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof(tkp), NULL, NULL))
	{
		OutputDebugString(_T("AdjustProcessTokenPrivilege AdjustTokenPrivileges Failed ! \n"));
		CloseHandle(hToken);
		return FALSE;
	}

	if (::GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		OutputDebugString(_T("The token does not have the specified privilege. \n"));
		CloseHandle(hToken);
		return FALSE;
	} 

	return true;
}

void InjectDll() 
{
	//HWND hWndLV = GetFirstChild(GetFirstChild(
	//	FindWindow(TEXT("ProgMan"), NULL)));
	//ASSERT(IsWindow(hWndLV));

	HWND hWndBeInjected = ::FindWindow(L"BeInjectedProg", NULL);
	/*HWND hWndBeInjected = GetFirstChild(GetFirstChild(
	FindWindow(TEXT("ProgMan"), NULL)));*/
	assert(hWndBeInjected);

	DWORD beInjectedThreadID = GetWindowThreadProcessId(hWndBeInjected, NULL);


	//// Set hook that injects our DLL into the Explorer's address space. After 
	//// setting the hook, the DIPS hidden modeless dialog box is created. We 
	//// send messages to this window to tell it what we want it to do.
	HRESULT hr = SetInjectionHook(beInjectedThreadID);
	assert(SUCCEEDED(hr));

	// Wait for the DIPS server window to be created.
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (msg.message == 0x282)
		{
			break;
		}
	}


	// Find the handle of the hidden dialog box window.
	HWND hWndDIPS = FindWindow(NULL, TEXT("MyDialog"));

	//// Make sure that the window was created.
	assert(IsWindow(hWndDIPS));

	// Tell the DIPS window which ListView window to manipulate
	// and whether the items should be saved or restored.
	//BOOL bSave = (cWhatToDo == TEXT('S'));
	SendMessage(hWndDIPS, WM_APP, (WPARAM) hWndBeInjected, 1);

	// Tell the DIPS window to destroy itself. Use SendMessage 
	// instead of PostMessage so that we know the window is 
	// destroyed before the hook is removed.
	SendMessage(hWndDIPS, WM_CLOSE, 0, 0);

	// Make sure that the window was destroyed.
	assert(!IsWindow(hWndDIPS));

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (msg.message == 0x282)
		{
			break;
		}
	}

	// Unhook the DLL, removing the DIPS dialog box procedure 
	// from the Explorer's address space.
	SetInjectionHook(0);  
}

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_ LPTSTR    lpCmdLine,
					   _In_ int       nCmdShow)
{
	AdjustProcessTokenPrivilege();

	// InjectDll();

	return 0;
}