// InjectionLib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "InjectionLib.h"
#include "resource.h"
#include <CommCtrl.h>
#include "..\BeInjectedProg\Resource.h"

// Instruct the compiler to put the g_hHook data variable in 
// its own data section called Shared. We then instruct the 
// linker that we want to share the data in this section 
// with all instances of this application.
#pragma data_seg("Shared")
HHOOK g_hHook = NULL;
DWORD g_dwThreadIdInjecter = 0;
#pragma data_seg()

// Instruct the linker to make the Shared section
// readable, writable, and shared.
#pragma comment(linker, "/section:Shared,rws")

// Nonshared variables
HINSTANCE g_hInstDll = NULL;

LRESULT WINAPI GetMsgProc(int nCode, WPARAM wParam, LPARAM lParam);
INT_PTR WINAPI Dlg_Proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);


#ifdef _DEBUG
// This function forces the debugger to be invoked
void ForceDebugBreak() {
	__try { DebugBreak(); }
	__except(UnhandledExceptionFilter(GetExceptionInformation())) { }
}
#else
#define ForceDebugBreak()
#endif

// This is an example of an exported function.
INJECTIONLIB_API HRESULT SetInjectionHook(DWORD dwThreadID)
{
	HRESULT hr = S_FALSE;

	if (dwThreadID != 0) 
	{
		// Make sure that the hook is not already installed.
		assert(g_hHook == NULL);

		// Save our thread ID in a shared variable so that our GetMsgProc 
		// function can post a message back to the thread when the server 
		// window has been created.
		g_dwThreadIdInjecter = GetCurrentThreadId();

		//CreateDialog(g_hInstDll, MAKEINTRESOURCE(IDD_MYDIALOG), NULL, Dlg_Proc);

		// Install the hook on the specified thread
		g_hHook = SetWindowsHookEx(WH_GETMESSAGE, GetMsgProc, g_hInstDll, 
			dwThreadID);

		hr = (g_hHook != NULL);
		if (hr) 
		{
			// The hook was installed successfully; force a begin message to 
			// the thread's queue so that the hook function gets called.
			hr = PostThreadMessage(dwThreadID, WM_NULL, 0, 0);
		}
	} 
	else 
	{
		// Make sure that a hook has been installed.
		assert(g_hHook != NULL);
		hr = UnhookWindowsHookEx(g_hHook);
		g_hHook = NULL;
	}

	return hr;
}

typedef LRESULT (CALLBACK* fpWindowProc)(_In_  HWND hwnd, _In_  UINT uMsg, _In_  WPARAM wParam, _In_  LPARAM lParam);
fpWindowProc g_oldWinProg = NULL;


LRESULT CALLBACK NewIEPage_WindowProc(
	_In_  HWND hwnd,
	_In_  UINT uMsg,
	_In_  WPARAM wParam,
	_In_  LPARAM lParam
	)
{
	int wmId, wmEvent;
	if (uMsg == WM_COMMAND)
	{
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			MessageBox(hwnd, _T("About Dialog"), NULL, 0);
			break;
		case IDM_EXIT:
			MessageBox(hwnd, _T("Exit"), NULL, 0);
			break;
		default:
			break;
		}
	}
	else if (uMsg == WM_DESTROY)
	{
		PostThreadMessage(g_dwThreadIdInjecter, 0x282, 0, 0);
	}

	if (g_oldWinProg)
	{
		return (*g_oldWinProg)(hwnd, uMsg, wParam, lParam);
	}
	else
	{
		return FALSE;
	}
}


LRESULT WINAPI GetMsgProc(int nCode, WPARAM wParam, LPARAM lParam) 
{
	static BOOL bFirstTime = TRUE;

	if (bFirstTime) 
	{
		// The DLL just got injected.
		bFirstTime = FALSE;
		// Uncomment the line below to invoke the debugger 
		// on the process that just got the injected DLL.
		//ForceDebugBreak();

		// DEBUG_INSERT_BREAK;

		HWND hWndBeInjected = ::FindWindow(L"BeInjectedProg", NULL);

		g_oldWinProg = (fpWindowProc)::SetWindowLong(hWndBeInjected, GWL_WNDPROC, (LONG)NewIEPage_WindowProc);

		// Create the DIPS Server window to handle the client request.
		CreateDialog(g_hInstDll, MAKEINTRESOURCE(IDD_MYDIALOG), NULL, Dlg_Proc);

		// Tell the DIPS application that the server is up 
		// and ready to handle requests.
		PostThreadMessage(g_dwThreadIdInjecter, 0x282, 0, 0);
	}

	return(CallNextHookEx(g_hHook, nCode, wParam, lParam));
}

void Dlg_OnClose(HWND hWnd) 
{
	DestroyWindow(hWnd);
}

INT_PTR WINAPI Dlg_Proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{

	switch (uMsg) 
	{
	case WM_CLOSE:
		Dlg_OnClose(hWnd);
		break;
	case WM_APP:
		// Uncomment the line below to invoke the debugger 
		// on the process that just got the injected DLL.
		//DEBUG_SHOW_MESSAGEBOX;
			
		break;
	default:
		break;
	}

	return(FALSE);
}