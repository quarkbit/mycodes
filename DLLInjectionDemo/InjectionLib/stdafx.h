// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <windowsx.h>
#include <tchar.h>
#include <assert.h>

#define ZY_DEBUG
#ifdef ZY_DEBUG
// ZY, Macros for debug
#include <atlbase.h>
#include <atlstr.h>

#define DEBUG_INSERT_BREAK if (IsDebuggerPresent()) {__asm{INT 3};}
#define DEBUG_SHOW_MESSAGEBOX do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	ATL::CString debugStr; \
	debugStr.Format(_T("ZYDEBUG, %S, Line: %d\nProcessID: %d[0x%x]\nThreadID: %d[0x%x]\n"), __FUNCDNAME__, __LINE__, proID, proID, threadID, threadID); \
	MessageBox(NULL, debugStr, _T("For Debug"), 0); \
} while(0)

#define DEBUG_OUTPUT_MESSAGE do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	ATL::CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, %s, %d\n"), proID, threadID, __FUNCTIONW__, __LINE__); \
	::OutputDebugString(debugStr); \
} while(0)

#define DEBUG_OUTPUT(FORMAT_STR, ...) do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	ATL::CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, "), proID, threadID); \
	debugStr.AppendFormat(FORMAT_STR, __VA_ARGS__); \
	::OutputDebugString(debugStr); \
} while(0)
#else
#define DEBUG_INSERT_BREAK
#define DEBUG_SHOW_MESSAGEBOX
#define DEBUG_OUTPUT_MESSAGE
#define DEBUG_OUTPUT
#endif

// TODO: reference additional headers your program requires here
