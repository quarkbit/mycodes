// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__8932F773_BCF2_437E_9586_C6DEFB709EF3__INCLUDED_)
#define AFX_STDAFX_H__8932F773_BCF2_437E_9586_C6DEFB709EF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define   _WIN32_WINNT   0x0500

// Insert your headers here
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <Winternl.h>

// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#define ZY_DEBUG
#ifdef ZY_DEBUG
// ZY, Macros for debug
#include <atlbase.h>
#include <atlstr.h>

#define DEBUG_INSERT_BREAK if (IsDebuggerPresent()) {__asm{INT 3};}
#define DEBUG_SHOW_MESSAGEBOX do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("ZYDEBUG, %S, Line: %d\nProcessID: %d[0x%x]\nThreadID: %d[0x%x]\n"), __FUNCDNAME__, __LINE__, proID, proID, threadID, threadID); \
	::MessageBox(NULL, debugStr, _T("For Debug"), 0); \
} while(0)

#define DEBUG_OUTPUT_MESSAGE do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, %s, %d\n"), proID, threadID, __FUNCTION__, __LINE__); \
	::OutputDebugString(debugStr); \
} while(0)

#define DEBUG_OUTPUT(FORMAT_STR, ...) do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, "), proID, threadID); \
	debugStr.AppendFormat(FORMAT_STR, __VA_ARGS__); \
	::OutputDebugString(debugStr); \
} while(0)

#define DEBUG_OUTPUT_ERROR_MESSAGE(err) do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, "), proID, threadID); \
	WCHAR buffer[0xff] = {0}; \
	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, err, 0, buffer, 0xff, NULL); \
	debugStr.AppendFormat(_T("%s"), buffer); \
	::OutputDebugString(debugStr); \
} while(0)

#else
#define DEBUG_INSERT_BREAK
#define DEBUG_SHOW_MESSAGEBOX
#define DEBUG_OUTPUT_MESSAGE
#define DEBUG_OUTPUT
#define DEBUG_OUTPUT_ERROR_MESSAGE
#endif


#endif // !defined(AFX_STDAFX_H__8932F773_BCF2_437E_9586_C6DEFB709EF3__INCLUDED_)
