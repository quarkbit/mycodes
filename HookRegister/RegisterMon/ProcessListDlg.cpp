// ProcessListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "RegisterMon.h"
#include "ProcessListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProcessListDlg dialog


CProcessListDlg::CProcessListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProcessListDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProcessListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CProcessListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProcessListDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProcessListDlg, CDialog)
	//{{AFX_MSG_MAP(CProcessListDlg)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, OnClickList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProcessListDlg message handlers

BOOL CProcessListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString s;
	SHFILEINFO shFileInfo = {0};

	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	
	m_list.InsertColumn(0, "进程名", LVCFMT_LEFT);
	m_list.InsertColumn(1, "PID", LVCFMT_LEFT);
	m_list.SetColumnWidth(0, 340);
	m_list.SetColumnWidth(1, 40);

	
	
	m_list.SetImageList(m_pImagelist, LVSIL_SMALL);



	int index = m_list.InsertItem(0, "不过滤");
	m_list.SetItemText(index, 1, "0");


	PROCESSENTRY32 info = {sizeof(PROCESSENTRY32)};
    HANDLE hProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    BOOL bReport = Process32First(hProcess,&info);
	
    while(bReport)
    {
		
		if (info.th32ProcessID > 0 && info.th32ProcessID != GetCurrentProcessId())
		{
			HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, info.th32ProcessID); 
			MODULEENTRY32 me = {sizeof(me)}; 
			for(BOOL fOk=Module32First(hSnapShot,&me); fOk; fOk=Module32Next(hSnapShot,&me)) 
			{ 
				if (strstr(me.szExePath, info.szExeFile) != NULL) 
				{ 
					::SHGetFileInfo(me.szExePath, 0, &shFileInfo, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX);
					index = m_list.InsertItem(0, me.szExePath, shFileInfo.iIcon);
					s.Format("%d", me.th32ProcessID);
					m_list.SetItemText(index, 1, s);
				} 
			} 

		}
      
        bReport = Process32Next(hProcess, &info);   
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE

	m_PID = 0;
}




void CProcessListDlg::OnClickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	CString s;
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nItem = m_list.GetNextSelectedItem(pos);
		s.Format("%s", m_list.GetItemText(nItem, 1));
		m_PID = atoi(s);
		m_ProcessName = m_list.GetItemText(nItem, 0);
	}
	
	*pResult = 0;
}
