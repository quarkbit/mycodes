// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__7544CA64_40BB_4AE1_9765_ED16CCB92181__INCLUDED_)
#define AFX_STDAFX_H__7544CA64_40BB_4AE1_9765_ED16CCB92181__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxcview.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#define ZY_DEBUG
#ifdef ZY_DEBUG
// ZY, Macros for debug
#include <atlbase.h>
#include <atlstr.h>

#define DEBUG_INSERT_BREAK if (IsDebuggerPresent()) {__asm{INT 3};}
#define DEBUG_SHOW_MESSAGEBOX do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("ZYDEBUG, %S, Line: %d\nProcessID: %d[0x%x]\nThreadID: %d[0x%x]\n"), __FUNCDNAME__, __LINE__, proID, proID, threadID, threadID); \
	::MessageBox(NULL, debugStr, _T("For Debug"), 0); \
} while(0)

#define DEBUG_OUTPUT_MESSAGE do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, %s, %d\n"), proID, threadID, __FUNCTIONW__, __LINE__); \
	::OutputDebugString(debugStr); \
} while(0)

#define DEBUG_OUTPUT(FORMAT_STR, ...) do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, "), proID, threadID); \
	debugStr.AppendFormat(FORMAT_STR, __VA_ARGS__); \
	::OutputDebugString(debugStr); \
} while(0)

#define DEBUG_OUTPUT_ERROR_MESSAGE(err) do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, "), proID, threadID); \
	WCHAR buffer[0xff] = {0}; \
	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, err, 0, buffer, 0xff, NULL); \
	debugStr.AppendFormat(_T("%s"), buffer); \
	::OutputDebugString(debugStr); \
} while(0)

#else
#define DEBUG_INSERT_BREAK
#define DEBUG_SHOW_MESSAGEBOX
#define DEBUG_OUTPUT_MESSAGE
#define DEBUG_OUTPUT
#define DEBUG_OUTPUT_ERROR_MESSAGE
#endif


#endif // !defined(AFX_STDAFX_H__7544CA64_40BB_4AE1_9765_ED16CCB92181__INCLUDED_)
