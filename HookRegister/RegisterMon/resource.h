//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by RegisterMon.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_REGISTTYPE                  129
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     131
#define IDC_LIST1                       1000
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_EDIT3                       1003
#define IDC_EDIT4                       1004
#define IDC_EDIT5                       1005
#define IDC_EDIT6                       1006
#define IDC_BUTTON1                     1007
#define IDC_BUTTON2                     1008
#define ID_FILE_START                   32771
#define ID_FILE_STOP                    32772
#define ID_FILE_CLEAR                   32773
#define ID_FILE_FILTER                  32777
#define ID_FILTER_RegCreateKeyEx        32778
#define ID_FILTER_RegSetValueEx         32779
#define ID_FILTER_RegDeleteKey          32780
#define ID_FILTER_RegDeleteValue        32781
#define ID_FILTER_RegQueryValueEx       32782
#define ID_FILTER_EXPLORER              32790
#define ID_VIEW_APP                     32791
#define ID_VIEW_REG                     32792
#define ID_FILE_DELETE                  32793
#define ID_FILTER_REGEDIT               32796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32797
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
