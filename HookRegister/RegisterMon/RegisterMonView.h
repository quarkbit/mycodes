// RegisterMonView.h : interface of the CRegisterMonView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_REGISTERMONVIEW_H__56D2A2D2_7F60_4A7B_AB92_4BC7415293A2__INCLUDED_)
#define AFX_REGISTERMONVIEW_H__56D2A2D2_7F60_4A7B_AB92_4BC7415293A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Tlhelp32.h>
#include "ProcessListDlg.h"
#include "InfoDlg.h"

#define		TYPE_RegCreateKeyExA	0
#define		TYPE_RegCreateKeyExW	1
#define		TYPE_RegSetValueExA		2
#define		TYPE_RegSetValueExW		3
#define		TYPE_RegDeleteKeyA		4
#define		TYPE_RegDeleteKeyW		5
#define		TYPE_RegDeleteValueA	6
#define		TYPE_RegDeleteValueW	7
#define		TYPE_RegQueryValueExA	8
#define		TYPE_RegQueryValueExW	9


#define		WM_MonPIDInvalid		WM_USER + 5642

struct PARAMS
{
	DWORD	PID;
	HKEY	hKey;
	char	buffer1[1024];
	int		cbBuffer1;
	char	buffer2[1024];
	int		cbBuffer2;
	DWORD	type;
	long	result;
};

typedef enum _KEY_INFORMATION_CLASS {  
	KeyBasicInformation,  
		KeyNodeInformation,
		KeyFullInformation,
		KeyNameInformation,  
		KeyCachedInformation,  
		KeyVirtualizationInformation
}
KEY_INFORMATION_CLASS;
typedef LONG(WINAPI*ZwQueryKey)(IN HANDLE, IN KEY_INFORMATION_CLASS, OUT PVOID, IN ULONG, OUT PULONG); 


class CRegisterMonView : public CListView
{
protected: // create from serialization only
	CRegisterMonView();
	DECLARE_DYNCREATE(CRegisterMonView)

// Attributes
public:
	CRegisterMonDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegisterMonView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRegisterMonView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRegisterMonView)
	afx_msg void OnFileStart();
	afx_msg void OnUpdateFileStart(CCmdUI* pCmdUI);
	afx_msg void OnFileStop();
	afx_msg void OnUpdateFileStop(CCmdUI* pCmdUI);
	afx_msg void OnFileClear();
	afx_msg void OnUpdateFileClear(CCmdUI* pCmdUI);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnFileFilter();
	afx_msg void OnUpdateFileFilter(CCmdUI* pCmdUI);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRegCreateKeyEx();
	afx_msg void OnUpdateRegCreateKeyEx(CCmdUI* pCmdUI);
	afx_msg void OnRegDeleteKey();
	afx_msg void OnUpdateRegDeleteKey(CCmdUI* pCmdUI);
	afx_msg void OnRegDeleteValue();
	afx_msg void OnUpdateRegDeleteValue(CCmdUI* pCmdUI);
	afx_msg void OnRegSetValueEx();
	afx_msg void OnUpdateRegSetValueEx(CCmdUI* pCmdUI);
	afx_msg void OnFilterExplorer();
	afx_msg void OnUpdateFilterExplorer(CCmdUI* pCmdUI);
	afx_msg void OnRegQueryValueEx();
	afx_msg void OnUpdateRegQueryValueEx(CCmdUI* pCmdUI);
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnFileDelete();
	afx_msg void OnUpdateFileDelete(CCmdUI* pCmdUI);
	afx_msg void OnViewApp();
	afx_msg void OnUpdateViewApp(CCmdUI* pCmdUI);
	afx_msg void OnViewReg();
	afx_msg void OnUpdateViewReg(CCmdUI* pCmdUI);
	afx_msg void OnFilterRegedit();
	afx_msg void OnUpdateFilterRegedit(CCmdUI* pCmdUI);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	afx_msg LRESULT OnUpdateFilter(WPARAM wp, LPARAM lp);
	DECLARE_MESSAGE_MAP()
private:
	BOOL ProcessPidToPath(DWORD ProcessId, PCHAR ProcessName, PCHAR ProcessPath);
	DWORD GetProcessID(PCHAR ProcessName);
	int WideToMultiByte(const WCHAR* wChar, char* mChar);
	void HKeyToKeyName(DWORD PID, HKEY hKey, char* pszKeyName);
private:
	void UpdateHookFunction();
	HHOOK	m_hHook;
	CImageList m_imglst;
	HMODULE m_Moudle;
	ZwQueryKey m_pZwQueryKey;
	CProcessListDlg* m_pDlg;

	BOOL	m_bRegCreateKeyEx;
	BOOL	m_bRegSetValueEx;
	BOOL	m_bRegDeleteKey;
	BOOL	m_bRegDeleteValue;
	BOOL	m_bRegQueryValueEx;
	BOOL	m_bIgnoreExplorer;
	BOOL	m_bIgnoreRegedit;
};

#ifndef _DEBUG  // debug version in RegisterMonView.cpp
inline CRegisterMonDoc* CRegisterMonView::GetDocument()
   { return (CRegisterMonDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGISTERMONVIEW_H__56D2A2D2_7F60_4A7B_AB92_4BC7415293A2__INCLUDED_)
