#if !defined(AFX_PROCESSLISTDLG_H__C280A925_16E1_4E7C_B0D3_9C7C39D3B474__INCLUDED_)
#define AFX_PROCESSLISTDLG_H__C280A925_16E1_4E7C_B0D3_9C7C39D3B474__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcessListDlg.h : header file
//

#include <Tlhelp32.h>

/////////////////////////////////////////////////////////////////////////////
// CProcessListDlg dialog

class CProcessListDlg : public CDialog
{
// Construction
public:
	CProcessListDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CProcessListDlg)
	enum { IDD = IDD_DIALOG1 };
	CListCtrl	m_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProcessListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CProcessListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClickList1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	DWORD m_PID;
	CString m_ProcessName;
	CImageList * m_pImagelist;
private:
	CImageList m_imglst;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCESSLISTDLG_H__C280A925_16E1_4E7C_B0D3_9C7C39D3B474__INCLUDED_)
