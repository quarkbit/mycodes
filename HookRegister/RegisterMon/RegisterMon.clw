; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CRegisterMonView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "RegisterMon.h"
LastPage=0

ClassCount=7
Class1=CRegisterMonApp
Class2=CRegisterMonDoc
Class3=CRegisterMonView
Class4=CMainFrame

ResourceCount=4
Resource1=IDD_ABOUTBOX
Class5=CAboutDlg
Resource2=IDR_MAINFRAME
Class6=CProcessListDlg
Resource3=IDD_DIALOG1
Class7=CInfoDlg
Resource4=IDD_DIALOG2

[CLS:CRegisterMonApp]
Type=0
HeaderFile=RegisterMon.h
ImplementationFile=RegisterMon.cpp
Filter=N

[CLS:CRegisterMonDoc]
Type=0
HeaderFile=RegisterMonDoc.h
ImplementationFile=RegisterMonDoc.cpp
Filter=N

[CLS:CRegisterMonView]
Type=0
HeaderFile=RegisterMonView.h
ImplementationFile=RegisterMonView.cpp
Filter=C
BaseClass=CListView
VirtualFilter=VWC
LastObject=ID_VIEW_REG


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=ID_FILTER_REGEDIT




[CLS:CAboutDlg]
Type=0
HeaderFile=RegisterMon.cpp
ImplementationFile=RegisterMon.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_STATIC,static,1342308352

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_START
Command2=ID_FILE_STOP
Command3=ID_FILE_SAVE
Command4=ID_FILE_DELETE
Command5=ID_FILE_CLEAR
Command6=ID_APP_EXIT
Command7=ID_FILTER_RegCreateKeyEx
Command8=ID_FILTER_RegSetValueEx
Command9=ID_FILTER_RegDeleteKey
Command10=ID_FILTER_RegDeleteValue
Command11=ID_FILTER_RegQueryValueEx
Command12=ID_FILE_FILTER
Command13=ID_FILTER_EXPLORER
Command14=ID_FILTER_REGEDIT
Command15=ID_VIEW_APP
Command16=ID_VIEW_REG
Command17=ID_APP_ABOUT
CommandCount=17

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_START
Command2=ID_FILE_STOP
Command3=ID_FILE_DELETE
Command4=ID_FILE_CLEAR
Command5=ID_FILE_FILTER
Command6=ID_VIEW_APP
Command7=ID_VIEW_REG
Command8=ID_APP_ABOUT
CommandCount=8

[DLG:IDD_DIALOG1]
Type=1
Class=CProcessListDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST1,SysListView32,1350631429

[CLS:CProcessListDlg]
Type=0
HeaderFile=ProcessListDlg.h
ImplementationFile=ProcessListDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CProcessListDlg
VirtualFilter=dWC

[DLG:IDD_DIALOG2]
Type=1
Class=CInfoDlg
ControlCount=12
Control1=IDC_EDIT1,edit,1342244992
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT2,edit,1342244992
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT3,edit,1342244992
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT4,edit,1342244992
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT5,edit,1342244992
Control11=IDC_STATIC,static,1342308352
Control12=IDC_EDIT6,edit,1342244992

[CLS:CInfoDlg]
Type=0
HeaderFile=InfoDlg.h
ImplementationFile=InfoDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CInfoDlg
VirtualFilter=dWC

