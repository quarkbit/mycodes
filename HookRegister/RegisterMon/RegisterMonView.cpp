// RegisterMonView.cpp : implementation of the CRegisterMonView class
//

#include "stdafx.h"
#include "RegisterMon.h"

#include "RegisterMonDoc.h"
#include "RegisterMonView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CRegisterMonView

IMPLEMENT_DYNCREATE(CRegisterMonView, CListView)

BEGIN_MESSAGE_MAP(CRegisterMonView, CListView)
	//{{AFX_MSG_MAP(CRegisterMonView)
	ON_COMMAND(ID_FILE_START, OnFileStart)
	ON_UPDATE_COMMAND_UI(ID_FILE_START, OnUpdateFileStart)
	ON_COMMAND(ID_FILE_STOP, OnFileStop)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP, OnUpdateFileStop)
	ON_COMMAND(ID_FILE_CLEAR, OnFileClear)
	ON_UPDATE_COMMAND_UI(ID_FILE_CLEAR, OnUpdateFileClear)
	ON_WM_COPYDATA()
	ON_COMMAND(ID_FILE_FILTER, OnFileFilter)
	ON_UPDATE_COMMAND_UI(ID_FILE_FILTER, OnUpdateFileFilter)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_FILTER_RegCreateKeyEx, OnRegCreateKeyEx)
	ON_UPDATE_COMMAND_UI(ID_FILTER_RegCreateKeyEx, OnUpdateRegCreateKeyEx)
	ON_COMMAND(ID_FILTER_RegDeleteKey, OnRegDeleteKey)
	ON_UPDATE_COMMAND_UI(ID_FILTER_RegDeleteKey, OnUpdateRegDeleteKey)
	ON_COMMAND(ID_FILTER_RegDeleteValue, OnRegDeleteValue)
	ON_UPDATE_COMMAND_UI(ID_FILTER_RegDeleteValue, OnUpdateRegDeleteValue)
	ON_COMMAND(ID_FILTER_RegSetValueEx, OnRegSetValueEx)
	ON_UPDATE_COMMAND_UI(ID_FILTER_RegSetValueEx, OnUpdateRegSetValueEx)
	ON_COMMAND(ID_FILTER_EXPLORER, OnFilterExplorer)
	ON_UPDATE_COMMAND_UI(ID_FILTER_EXPLORER, OnUpdateFilterExplorer)
	ON_COMMAND(ID_FILTER_RegQueryValueEx, OnRegQueryValueEx)
	ON_UPDATE_COMMAND_UI(ID_FILTER_RegQueryValueEx, OnUpdateRegQueryValueEx)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND(ID_FILE_DELETE, OnFileDelete)
	ON_UPDATE_COMMAND_UI(ID_FILE_DELETE, OnUpdateFileDelete)
	ON_COMMAND(ID_VIEW_APP, OnViewApp)
	ON_UPDATE_COMMAND_UI(ID_VIEW_APP, OnUpdateViewApp)
	ON_COMMAND(ID_VIEW_REG, OnViewReg)
	ON_UPDATE_COMMAND_UI(ID_VIEW_REG, OnUpdateViewReg)
	ON_COMMAND(ID_FILTER_REGEDIT, OnFilterRegedit)
	ON_UPDATE_COMMAND_UI(ID_FILTER_REGEDIT, OnUpdateFilterRegedit)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_MonPIDInvalid, OnUpdateFilter)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonView construction/destruction

CRegisterMonView::CRegisterMonView()
{
	// TODO: add construction code here
	m_pDlg = NULL;
	m_bRegCreateKeyEx = FALSE;
	m_bRegSetValueEx = TRUE;
	m_bRegDeleteKey = TRUE;
	m_bRegDeleteValue = TRUE;
	m_bRegQueryValueEx = FALSE;
	m_bIgnoreExplorer = TRUE;
	m_bIgnoreRegedit = TRUE;

	m_hHook = NULL;
}

CRegisterMonView::~CRegisterMonView()
{
}

BOOL CRegisterMonView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CListView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonView drawing

void CRegisterMonView::OnDraw(CDC* pDC)
{
	CRegisterMonDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

void CRegisterMonView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();


	// TODO: You may populate your ListView with items by directly accessing
	//  its list control through a call to GetListCtrl().
	CListCtrl& theCtrl = GetListCtrl();
	theCtrl.ModifyStyle(0, LVS_REPORT | LVS_SINGLESEL);
	theCtrl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	
	
	// Insert a column. This override is the most convenient.
	theCtrl.InsertColumn(0, "进程名", LVCFMT_LEFT);
	theCtrl.InsertColumn(1, "行为", LVCFMT_LEFT);
	theCtrl.InsertColumn(2, "键名", LVCFMT_LEFT);
	theCtrl.InsertColumn(3, "名称", LVCFMT_LEFT);
	theCtrl.InsertColumn(4, "数据", LVCFMT_LEFT);
	theCtrl.InsertColumn(5, "类型", LVCFMT_LEFT);
	
	
	// Set reasonable widths for our columns
	theCtrl.SetColumnWidth(0, 110);
	theCtrl.SetColumnWidth(1, 100);
	theCtrl.SetColumnWidth(2, 250);
	theCtrl.SetColumnWidth(3, 110);
	theCtrl.SetColumnWidth(4, 110);
	theCtrl.SetColumnWidth(5, 70);

	m_Moudle = LoadLibrary("Hook.dll");


	HIMAGELIST m_hSmallImageList;
	typedef BOOL (WINAPI * SHGIL_PROC)	(HIMAGELIST *phLarge, HIMAGELIST *phSmall);
	HMODULE hShell32 = LoadLibrary("shell32.dll");
	SHGIL_PROC Shell_GetImageLists = (SHGIL_PROC)GetProcAddress(hShell32, (LPCSTR)71);
	
	Shell_GetImageLists(NULL, &m_hSmallImageList);
	
	if(m_hSmallImageList != NULL) 
		m_imglst.Attach(m_hSmallImageList);
	
	theCtrl.SetImageList(&m_imglst, LVSIL_SMALL);


	m_pZwQueryKey = (ZwQueryKey)::GetProcAddress(LoadLibrary("ntdll.dll"), "ZwQueryKey");
}

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonView diagnostics

#ifdef _DEBUG
void CRegisterMonView::AssertValid() const
{
	CListView::AssertValid();
}

void CRegisterMonView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CRegisterMonDoc* CRegisterMonView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRegisterMonDoc)));
	return (CRegisterMonDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonView message handlers

void CRegisterMonView::OnFileStart() 
{
	if(m_Moudle)
	{
		typedef HHOOK (*InstallHook)(HWND, DWORD);
		InstallHook pFunc = (InstallHook)::GetProcAddress(m_Moudle, "InstallHook");
		if(pFunc)
			m_hHook = (*pFunc)(m_hWnd, 0);

		
		DWORD pid = 0;
		if(m_bIgnoreExplorer)
			pid = GetProcessID("explorer.exe");
		typedef void (*SetExplorerPID)(DWORD);
		SetExplorerPID pFunc2 = (SetExplorerPID)::GetProcAddress(m_Moudle, "SetExplorerPID");
		if(pFunc2)
			(*pFunc2)(pid);
		
		UpdateHookFunction();
	}
}

void CRegisterMonView::OnUpdateFileStart(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_hHook == NULL);
}

void CRegisterMonView::OnFileStop() 
{
	GetOwner()->SetWindowText("注册表监控程序");
	if(m_Moudle)
	{
		typedef BOOL (*UninstallHook)();
		UninstallHook pFunc = (UninstallHook)::GetProcAddress(m_Moudle, "UninstallHook");
		if(pFunc)
		{
			if((*pFunc)())
				m_hHook = NULL;
		}
	}
}

void CRegisterMonView::OnUpdateFileStop(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_hHook != NULL);
}

void CRegisterMonView::OnFileClear() 
{
	CListCtrl& theCtrl = GetListCtrl();
	if (IDYES == MessageBox("是否要清空列表", "提示", MB_YESNO | MB_ICONQUESTION))
		theCtrl.DeleteAllItems();
}

void CRegisterMonView::OnUpdateFileClear(CCmdUI* pCmdUI) 
{
	CListCtrl& theCtrl = GetListCtrl();
	pCmdUI->Enable(theCtrl.GetItemCount());
}

void CRegisterMonView::OnFileFilter() 
{
	CProcessListDlg dlg;
	dlg.m_pImagelist = &m_imglst;
	if(IDOK == dlg.DoModal())
	{
		if(m_Moudle)
		{
			if(dlg.m_PID == 0)
				GetOwner()->SetWindowText("注册表监控程序");
			else
				GetOwner()->SetWindowText("注册表监控程序(" + dlg.m_ProcessName + ")");
			typedef void (*SetFilter)(DWORD);
			SetFilter pFunc = (SetFilter)::GetProcAddress(m_Moudle, "SetFilter");
			if(pFunc)
				(*pFunc)(dlg.m_PID);
		}

	}
}

void CRegisterMonView::OnUpdateFileFilter(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable(m_hHook != NULL);
}
int CRegisterMonView::WideToMultiByte(const WCHAR* wChar, char* mChar)
{
	int aLen = WideCharToMultiByte(CP_ACP, 0, wChar, wcslen(wChar) + 1, NULL, 0, NULL, NULL);
	return WideCharToMultiByte(CP_ACP, 0, wChar, wcslen(wChar) + 1, mChar, aLen, NULL, NULL);
}

DWORD CRegisterMonView::GetProcessID(PCHAR ProcessName)
{
	CString s1;
	CString s2;
	PROCESSENTRY32 info = {sizeof(PROCESSENTRY32)};
    HANDLE hProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    BOOL bReport = Process32First(hProcess,&info);
	
    while(bReport)
    {
		s1 = ProcessName;
		s2 = info.szExeFile;
		s1.MakeUpper();
		s2.MakeUpper();
		if(s1 == s2)
			return info.th32ProcessID;
        bReport = Process32Next(hProcess, &info);   
    }
	return 0;
}

BOOL CRegisterMonView::ProcessPidToPath(DWORD ProcessId, PCHAR ProcessName, PCHAR ProcessPath)
{
	PROCESSENTRY32 info = {sizeof(PROCESSENTRY32)};
    MODULEENTRY32 minfo = {sizeof(MODULEENTRY32)};
    HANDLE hProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    BOOL bReport = Process32First(hProcess,&info);
	
    while(bReport)
    {
		if(info.th32ProcessID == ProcessId)
		{
			HANDLE hModule = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, info.th32ProcessID);
			Module32First(hModule, &minfo);
			strcpy(ProcessName, info.szExeFile);
			strcpy(ProcessPath, minfo.szExePath);
			return true;
		}
        bReport = Process32Next(hProcess, &info);   
    }
	strcpy(ProcessName, "");
	strcpy(ProcessPath, "");
	return false;
}

CString myFunc(char* pString)
{
	CString s = pString;
	bool flag = false;
	if(s.Find("\\REGISTRY\\USER") == 0)
		flag = true;
	s.Replace("\\REGISTRY\\USER", "HKEY_CURRENT_USER");
	s.Replace("\\REGISTRY\\MACHINE\\SOFTWARE\\Classes", "HKEY_CLASSES_ROOT");
	s.Replace("\\REGISTRY\\MACHINE", "HKEY_LOCAL_MACHINE");
	if(flag)
	{
		int a1 = s.Find("\\");
		CString s1 = s.Mid(0, a1+1);
		CString s2 = s.Mid(a1+1);
		int a2 = s2.Find("\\");
		CString s3 = s2.Mid(a2+1);
		if(a2 == -1)
			s = "HKEY_CURRENT_USER";
		else
			s = s1 + s3;
	}
	return s;
}

void CRegisterMonView::HKeyToKeyName(DWORD PID, HKEY hKey, char* pszKeyName)
{
	HANDLE hRemoteProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID);
	
	HANDLE hKey2;
	DuplicateHandle(
		hRemoteProcess, 
		HANDLE(hKey),
		GetCurrentProcess(), 
		&hKey2, 
		0,
		FALSE,
		DUPLICATE_SAME_ACCESS);

	WCHAR wszText[1024] = {0};
	ULONG len = 0;
	if(m_pZwQueryKey)
	{
		(*m_pZwQueryKey)(hKey2, KeyNameInformation, wszText, 1024, &len);
		WideToMultiByte(wszText+2, pszKeyName);
		//MessageBox(pszKeyName);
		CString s = myFunc(pszKeyName);
		strcpy(pszKeyName, s);
	}
}

BOOL CRegisterMonView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	SHFILEINFO shFileInfo = {0};

	CListCtrl& theCtrl = GetListCtrl();
	
	CString str = "";
	char szTemp[1024];
	char szTemp2[1024];
	
	PARAMS* params = (PARAMS*)pCopyDataStruct->lpData;


	char szPath[MAX_PATH];
	char szName[MAX_PATH];
	ProcessPidToPath(params->PID, szName, szPath);
	::SHGetFileInfo(szPath, 0, &shFileInfo, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX);
	if (m_bIgnoreRegedit)
	{
		CString strTemp = szName;
		strTemp.MakeLower();
		if (strTemp.Find("regedit.exe") > -1)
			return TRUE;
	}
	int index = theCtrl.InsertItem(0xFFFF, szPath, shFileInfo.iIcon);

	switch (pCopyDataStruct->dwData)
	{
	///////////////////////////////////////////////////////////////////////
	case TYPE_RegCreateKeyExA:
		str = "RegCreateKeyExA";
		theCtrl.SetItemText(index, 1, str);
		
		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str.Format("HKEY_CLASSES_ROOT\\%s", params->buffer1);
			break;
		case HKEY_CURRENT_USER:
			str.Format("HKEY_CURRENT_USER\\%s", params->buffer1);
			break;
		case HKEY_LOCAL_MACHINE:
			str.Format("HKEY_LOCAL_MACHINE\\%s", params->buffer1);
			break;
		case HKEY_USERS:
			str.Format("HKEY_USERS\\%s", params->buffer1);
			break;
		case HKEY_PERFORMANCE_DATA:
			str.Format("HKEY_PERFORMANCE_DATA\\%s", params->buffer1);
			break;
		case HKEY_CURRENT_CONFIG:
			str.Format("HKEY_CURRENT_CONFIG\\%s", params->buffer1);
			break;
		case HKEY_DYN_DATA:
			str.Format("HKEY_DYN_DATA\\%s", params->buffer1);
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s\\%s", szTemp2, params->buffer1);
		}
		theCtrl.SetItemText(index, 2, str);
		theCtrl.SetItemText(index, 3, "N/A");
		theCtrl.SetItemText(index, 4, "N/A");
		theCtrl.SetItemText(index, 5, "N/A");
		break;
	case TYPE_RegCreateKeyExW:
		str = "RegCreateKeyExW";
		theCtrl.SetItemText(index, 1, str);
		
		WideToMultiByte((WCHAR*)params->buffer1, szTemp);
		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str.Format("HKEY_CLASSES_ROOT\\%s", szTemp);
			break;
		case HKEY_CURRENT_USER:
			str.Format("HKEY_CURRENT_USER\\%s", szTemp);
			break;
		case HKEY_LOCAL_MACHINE:
			str.Format("HKEY_LOCAL_MACHINE\\%s", szTemp);
			break;
		case HKEY_USERS:
			str.Format("HKEY_USERS\\%s", szTemp);
			break;
		case HKEY_PERFORMANCE_DATA:
			str.Format("HKEY_PERFORMANCE_DATA\\%s", szTemp);
			break;
		case HKEY_CURRENT_CONFIG:
			str.Format("HKEY_CURRENT_CONFIG\\%s", szTemp);
			break;
		case HKEY_DYN_DATA:
			str.Format("HKEY_DYN_DATA\\%s", szTemp);
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s\\%s", szTemp2, szTemp);
		}
		theCtrl.SetItemText(index, 2, str);
		theCtrl.SetItemText(index, 3, "N/A");
		theCtrl.SetItemText(index, 4, "N/A");
		theCtrl.SetItemText(index, 5, "N/A");
		break;

	///////////////////////////////////////////////////////////////////////
	case TYPE_RegSetValueExA:
		str = "RegSetValueExA";
		theCtrl.SetItemText(index, 1, str);
		HKeyToKeyName(params->PID, params->hKey, szTemp2);
		theCtrl.SetItemText(index, 2, szTemp2);
		//theCtrl.SetItemText(index, 3, params->buffer1);
		if(params->buffer1 == 0 || strlen(params->buffer1) == 0)
			theCtrl.SetItemText(index, 3, "默认值");
		else
			theCtrl.SetItemText(index, 3, params->buffer1);

		switch (params->type)
		{
		case REG_NONE:
			theCtrl.SetItemText(index, 5, "REG_NONE");
			//str.Format("%s", params->buffer2);
			break;
		case REG_SZ:
			theCtrl.SetItemText(index, 5, "REG_SZ");
			str.Format("%s", params->buffer2);
			break;
		case REG_EXPAND_SZ:
			theCtrl.SetItemText(index, 5, "REG_EXPAND_SZ");
			str.Format("%s", params->buffer2);
			break;
		case REG_BINARY:
			theCtrl.SetItemText(index, 5, "REG_BINARY");
			str = "";
			int i;
			for(i=0; i<params->cbBuffer2; i++)
			{
				str.Format("%s%02X ", str, *((BYTE*)params->buffer2 + i));
			}
			break;
		case REG_DWORD:
			theCtrl.SetItemText(index, 5, "REG_DWORD");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_DWORD_BIG_ENDIAN:
			theCtrl.SetItemText(index, 5, "REG_DWORD_BIG_ENDIAN");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_LINK:
			theCtrl.SetItemText(index, 5, "REG_LINK");
			break;
		case REG_MULTI_SZ:
			theCtrl.SetItemText(index, 5, "REG_MULTI_SZ");
			break;
		case REG_RESOURCE_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_LIST");
			break;
		case REG_FULL_RESOURCE_DESCRIPTOR:
			theCtrl.SetItemText(index, 5, "REG_FULL_RESOURCE_DESCRIPTOR");
			break;
		case REG_RESOURCE_REQUIREMENTS_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_REQUIREMENTS_LIST");
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s\\%s", szTemp2, params->buffer2);
			break;
		}
		
		theCtrl.SetItemText(index, 4, str);
		
		break;

	case TYPE_RegSetValueExW:
		str = "RegSetValueExW";
		theCtrl.SetItemText(index, 1, str);
		
		HKeyToKeyName(params->PID, params->hKey, szTemp2);
		theCtrl.SetItemText(index, 2, szTemp2);

		WideToMultiByte((WCHAR*)params->buffer1, szTemp);
		//theCtrl.SetItemText(index, 3, szTemp);
		if(params->buffer1 == 0 || strlen(params->buffer1) == 0)
			theCtrl.SetItemText(index, 3, "默认值");
		else
			theCtrl.SetItemText(index, 3, szTemp);

		WideToMultiByte((WCHAR*)params->buffer2, szTemp);
		switch (params->type)
		{
		case REG_NONE:
			theCtrl.SetItemText(index, 5, "REG_NONE");
			//str.Format("%s", szTemp);
			break;
		case REG_SZ:
			theCtrl.SetItemText(index, 5, "REG_SZ");
			str.Format("%s", szTemp);
			break;
		case REG_EXPAND_SZ:
			theCtrl.SetItemText(index, 5, "REG_EXPAND_SZ");
			str.Format("%s", szTemp);
			break;
		case REG_BINARY:
			theCtrl.SetItemText(index, 5, "REG_BINARY");
			str = "";
			int i;
			for(i=0; i<params->cbBuffer2; i++)
			{
				str.Format("%s%02X ", str, *((BYTE*)params->buffer2 + i));
			}
			break;
		case REG_DWORD:
			theCtrl.SetItemText(index, 5, "REG_DWORD");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_DWORD_BIG_ENDIAN:
			theCtrl.SetItemText(index, 5, "REG_DWORD_BIG_ENDIAN");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_LINK:
			theCtrl.SetItemText(index, 5, "REG_LINK");
			break;
		case REG_MULTI_SZ:
			theCtrl.SetItemText(index, 5, "REG_MULTI_SZ");
			break;
		case REG_RESOURCE_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_LIST");
			break;
		case REG_FULL_RESOURCE_DESCRIPTOR:
			theCtrl.SetItemText(index, 5, "REG_FULL_RESOURCE_DESCRIPTOR");
			break;
		case REG_RESOURCE_REQUIREMENTS_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_REQUIREMENTS_LIST");
			break;
		default:
			break;
		}

		theCtrl.SetItemText(index, 4, str);
		break;
		///////////////////////////////////////////////////////////////////////

	case TYPE_RegDeleteKeyA:
		str = "RegDeleteKeyA";
		theCtrl.SetItemText(index, 1, str);

		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str.Format("HKEY_CLASSES_ROOT\\%s", params->buffer1);
			break;
		case HKEY_CURRENT_USER:
			str.Format("HKEY_CURRENT_USER\\%s", params->buffer1);
			break;
		case HKEY_LOCAL_MACHINE:
			str.Format("HKEY_LOCAL_MACHINE\\%s", params->buffer1);
			break;
		case HKEY_USERS:
			str.Format("HKEY_USERS\\%s", params->buffer1);
			break;
		case HKEY_PERFORMANCE_DATA:
			str.Format("HKEY_PERFORMANCE_DATA\\%s", params->buffer1);
			break;
		case HKEY_CURRENT_CONFIG:
			str.Format("HKEY_CURRENT_CONFIG\\%s", params->buffer1);
			break;
		case HKEY_DYN_DATA:
			str.Format("HKEY_DYN_DATA\\%s", params->buffer1);
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s\\%s", szTemp2, params->buffer1);
		}
		theCtrl.SetItemText(index, 2, str);
		theCtrl.SetItemText(index, 3, "N/A");
		theCtrl.SetItemText(index, 4, "N/A");
		theCtrl.SetItemText(index, 5, "N/A");
		break;

	case TYPE_RegDeleteKeyW:
		str = "RegDeleteKeyW";
		theCtrl.SetItemText(index, 1, str);
		
		WideToMultiByte((WCHAR*)params->buffer1, szTemp);
		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str.Format("HKEY_CLASSES_ROOT\\%s", szTemp);
			break;
		case HKEY_CURRENT_USER:
			str.Format("HKEY_CURRENT_USER\\%s", szTemp);
			break;
		case HKEY_LOCAL_MACHINE:
			str.Format("HKEY_LOCAL_MACHINE\\%s", szTemp);
			break;
		case HKEY_USERS:
			str.Format("HKEY_USERS\\%s", szTemp);
			break;
		case HKEY_PERFORMANCE_DATA:
			str.Format("HKEY_PERFORMANCE_DATA\\%s", szTemp);
			break;
		case HKEY_CURRENT_CONFIG:
			str.Format("HKEY_CURRENT_CONFIG\\%s", szTemp);
			break;
		case HKEY_DYN_DATA:
			str.Format("HKEY_DYN_DATA\\%s", szTemp);
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s\\%s", szTemp2, szTemp);
		}
		theCtrl.SetItemText(index, 2, str);
		theCtrl.SetItemText(index, 3, "N/A");
		theCtrl.SetItemText(index, 4, "N/A");
		theCtrl.SetItemText(index, 5, "N/A");
		break;

		///////////////////////////////////////////////////////////////////////
	case TYPE_RegDeleteValueA:
		str = "RegDeleteValueA";
		theCtrl.SetItemText(index, 1, str);
		
		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str = "HKEY_CLASSES_ROOT";
			break;
		case HKEY_CURRENT_USER:
			str = "HKEY_CURRENT_USER";
			break;
		case HKEY_LOCAL_MACHINE:
			str = "HKEY_LOCAL_MACHINE";
			break;
		case HKEY_USERS:
			str = "HKEY_USERS";
			break;
		case HKEY_PERFORMANCE_DATA:
			str = "HKEY_PERFORMANCE_DATA";
			break;
		case HKEY_CURRENT_CONFIG:
			str = "HKEY_CURRENT_CONFIG";
			break;
		case HKEY_DYN_DATA:
			str = "HKEY_DYN_DATA";
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s", szTemp2);
		}
		theCtrl.SetItemText(index, 2, str);

		if(params->buffer1 == 0 || strlen(params->buffer1) == 0)
			theCtrl.SetItemText(index, 3, "默认值");
		else
			theCtrl.SetItemText(index, 3, params->buffer1);

		theCtrl.SetItemText(index, 4, "N/A");
		theCtrl.SetItemText(index, 5, "N/A");
		break;
	case TYPE_RegDeleteValueW:
		str = "RegDeleteValueW";
		theCtrl.SetItemText(index, 1, str);
		
		WideToMultiByte((WCHAR*)params->buffer1, szTemp);
		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str = "HKEY_CLASSES_ROOT";
			break;
		case HKEY_CURRENT_USER:
			str = "HKEY_CURRENT_USER";
			break;
		case HKEY_LOCAL_MACHINE:
			str = "HKEY_LOCAL_MACHINE";
			break;
		case HKEY_USERS:
			str = "HKEY_USERS";
			break;
		case HKEY_PERFORMANCE_DATA:
			str = "HKEY_PERFORMANCE_DATA";
			break;
		case HKEY_CURRENT_CONFIG:
			str = "HKEY_CURRENT_CONFIG";
			break;
		case HKEY_DYN_DATA:
			str = "HKEY_DYN_DATA";
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s", szTemp2);
		}
		theCtrl.SetItemText(index, 2, str);

		WideToMultiByte((WCHAR*)params->buffer1, szTemp);
		if(params->buffer1 == 0 || strlen(params->buffer1) == 0)
			theCtrl.SetItemText(index, 3, "默认值");
		else
			theCtrl.SetItemText(index, 3, szTemp);

		theCtrl.SetItemText(index, 4, "N/A");
		theCtrl.SetItemText(index, 5, "N/A");
		break;

		///////////////////////////////////////////////////////////////////////
	case TYPE_RegQueryValueExA:
		str = "RegQueryValueExA";
		theCtrl.SetItemText(index, 1, str);
		
		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str = "HKEY_CLASSES_ROOT";
			break;
		case HKEY_CURRENT_USER:
			str = "HKEY_CURRENT_USER";
			break;
		case HKEY_LOCAL_MACHINE:
			str = "HKEY_LOCAL_MACHINE";
			break;
		case HKEY_USERS:
			str = "HKEY_USERS";
			break;
		case HKEY_PERFORMANCE_DATA:
			str = "HKEY_PERFORMANCE_DATA";
			break;
		case HKEY_CURRENT_CONFIG:
			str = "HKEY_CURRENT_CONFIG";
			break;
		case HKEY_DYN_DATA:
			str = "HKEY_DYN_DATA";
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s", szTemp2);
		}
		theCtrl.SetItemText(index, 2, str);
		
		if(params->buffer1 == 0 || strlen(params->buffer1) == 0)
			theCtrl.SetItemText(index, 3, "默认值");
		else
			theCtrl.SetItemText(index, 3, params->buffer1);
		
		switch (params->type)
		{
		case REG_NONE:
			theCtrl.SetItemText(index, 5, "REG_NONE");
			if(params->cbBuffer2 == 4)
				str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			else
			{
				if (params->cbBuffer2 == (int)strlen(params->buffer2)+1)
					str.Format("%s", params->buffer2);
				else
				{
					str = "";
					int i;
					for(i=0; i<params->cbBuffer2; i++)
					{
						str.Format("%s%02X ", str, *((BYTE*)params->buffer2 + i));
					}
				}
			}
			break;
		case REG_SZ:
			theCtrl.SetItemText(index, 5, "REG_SZ");
			str.Format("%s", params->buffer2);
			break;
		case REG_EXPAND_SZ:
			theCtrl.SetItemText(index, 5, "REG_EXPAND_SZ");
			str.Format("%s", params->buffer2);
			break;
		case REG_BINARY:
			theCtrl.SetItemText(index, 5, "REG_BINARY");
			str = "";
			int i;
			for(i=0; i<params->cbBuffer2; i++)
			{
				str.Format("%s%02X ", str, *((BYTE*)params->buffer2 + i));
			}
			break;
		case REG_DWORD:
			theCtrl.SetItemText(index, 5, "REG_DWORD");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_DWORD_BIG_ENDIAN:
			theCtrl.SetItemText(index, 5, "REG_DWORD_BIG_ENDIAN");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_LINK:
			theCtrl.SetItemText(index, 5, "REG_LINK");
			break;
		case REG_MULTI_SZ:
			theCtrl.SetItemText(index, 5, "REG_MULTI_SZ");
			break;
		case REG_RESOURCE_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_LIST");
			break;
		case REG_FULL_RESOURCE_DESCRIPTOR:
			theCtrl.SetItemText(index, 5, "REG_FULL_RESOURCE_DESCRIPTOR");
			break;
		case REG_RESOURCE_REQUIREMENTS_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_REQUIREMENTS_LIST");
			break;
		default:
			CString sTemp;
			sTemp.Format("%d", params->type);
			theCtrl.SetItemText(index, 5, sTemp);
			
			str = "";
			break;
		}
		theCtrl.SetItemText(index, 4, str);
		break;

	case TYPE_RegQueryValueExW:
		str = "RegQueryValueExW";
		theCtrl.SetItemText(index, 1, str);

		switch (int(params->hKey))
		{
		case HKEY_CLASSES_ROOT:
			str = "HKEY_CLASSES_ROOT";
			break;
		case HKEY_CURRENT_USER:
			str = "HKEY_CURRENT_USER";
			break;
		case HKEY_LOCAL_MACHINE:
			str = "HKEY_LOCAL_MACHINE";
			break;
		case HKEY_USERS:
			str = "HKEY_USERS";
			break;
		case HKEY_PERFORMANCE_DATA:
			str = "HKEY_PERFORMANCE_DATA";
			break;
		case HKEY_CURRENT_CONFIG:
			str = "HKEY_CURRENT_CONFIG";
			break;
		case HKEY_DYN_DATA:
			str = "HKEY_DYN_DATA";
			break;
		default:
			HKeyToKeyName(params->PID, params->hKey, szTemp2);
			str.Format("%s", szTemp2);
		}
		theCtrl.SetItemText(index, 2, str);
		
		WideToMultiByte((WCHAR*)params->buffer1, szTemp);
		if(params->buffer1 == 0 || strlen(params->buffer1) == 0)
			theCtrl.SetItemText(index, 3, "默认值");
		else
			theCtrl.SetItemText(index, 3, szTemp);
		
		WideToMultiByte((WCHAR*)params->buffer2, szTemp);
		switch (params->type)
		{
		case REG_NONE:
			theCtrl.SetItemText(index, 5, "REG_NONE");
			if(params->cbBuffer2 == 4)
				str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			else
			{
				if (params->cbBuffer2 == (int)wcslen((WCHAR*)params->buffer2)*2+2)
					str.Format("%s", szTemp);
				else
				{
					str = "";
					int i;
					for(i=0; i<params->cbBuffer2; i++)
					{
						str.Format("%s%02X ", str, *((BYTE*)params->buffer2 + i));
					}
				}
			}
			break;
		case REG_SZ:
			theCtrl.SetItemText(index, 5, "REG_SZ");
			str.Format("%s", szTemp);
			break;
		case REG_EXPAND_SZ:
			theCtrl.SetItemText(index, 5, "REG_EXPAND_SZ");
			str.Format("%s", szTemp);
			break;
		case REG_BINARY:
			theCtrl.SetItemText(index, 5, "REG_BINARY");
			str = "";
			int i;
			for(i=0; i<params->cbBuffer2; i++)
			{
				str.Format("%s%02X ", str, *((BYTE*)params->buffer2 + i));
			}
			break;
		case REG_DWORD:
			theCtrl.SetItemText(index, 5, "REG_DWORD");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_DWORD_BIG_ENDIAN:
			theCtrl.SetItemText(index, 5, "REG_DWORD_BIG_ENDIAN");
			str.Format("0x%08X(%d)", *((DWORD*)params->buffer2), *((DWORD*)params->buffer2));
			break;
		case REG_LINK:
			theCtrl.SetItemText(index, 5, "REG_LINK");
			break;
		case REG_MULTI_SZ:
			theCtrl.SetItemText(index, 5, "REG_MULTI_SZ");
			break;
		case REG_RESOURCE_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_LIST");
			break;
		case REG_FULL_RESOURCE_DESCRIPTOR:
			theCtrl.SetItemText(index, 5, "REG_FULL_RESOURCE_DESCRIPTOR");
			break;
		case REG_RESOURCE_REQUIREMENTS_LIST:
			theCtrl.SetItemText(index, 5, "REG_RESOURCE_REQUIREMENTS_LIST");
			break;
		default:
			CString sTemp;
			sTemp.Format("%d", params->type);
			theCtrl.SetItemText(index, 5, sTemp);
			str = "";
			break;
		}
		theCtrl.SetItemText(index, 4, str);

		break;

		///////////////////////////////////////////////////////////////////////
	default:
		break;
	}
	theCtrl.Scroll(CSize(0,10));
	return CListView::OnCopyData(pWnd, pCopyDataStruct);
}







void CRegisterMonView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	if (pos)
	{
		CInfoDlg dlg;
		int nItem = theCtrl.GetNextSelectedItem(pos);
		dlg.s1 = theCtrl.GetItemText(nItem, 0);
		dlg.s2 = theCtrl.GetItemText(nItem, 1);
		dlg.s3 = theCtrl.GetItemText(nItem, 2);
		dlg.s4 = theCtrl.GetItemText(nItem, 3);
		dlg.s5 = theCtrl.GetItemText(nItem, 4);
		dlg.s6 = theCtrl.GetItemText(nItem, 5);
		dlg.DoModal();
	}
	CListView::OnLButtonDblClk(nFlags, point);
}

LRESULT CRegisterMonView::OnUpdateFilter(WPARAM wp, LPARAM lp)
{
	GetOwner()->SetWindowText("注册表监控程序");
	MessageBox("你设置的过滤器已过期", "友情提示", MB_OK | MB_ICONINFORMATION);
	return 0;
}


void CRegisterMonView::OnRegCreateKeyEx() 
{
	m_bRegCreateKeyEx = !m_bRegCreateKeyEx;
	UpdateHookFunction();
}

void CRegisterMonView::OnUpdateRegCreateKeyEx(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bRegCreateKeyEx);
}

void CRegisterMonView::OnRegDeleteKey() 
{
	m_bRegDeleteKey = !m_bRegDeleteKey;
	UpdateHookFunction();
}

void CRegisterMonView::OnUpdateRegDeleteKey(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bRegDeleteKey);
}

void CRegisterMonView::OnRegDeleteValue() 
{
	m_bRegDeleteValue = !m_bRegDeleteValue;
	UpdateHookFunction();
}

void CRegisterMonView::OnUpdateRegDeleteValue(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bRegDeleteValue);	
}

void CRegisterMonView::OnRegSetValueEx() 
{
	m_bRegSetValueEx = !m_bRegSetValueEx;
	UpdateHookFunction();
}

void CRegisterMonView::OnUpdateRegSetValueEx(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bRegSetValueEx);	
}

void CRegisterMonView::OnRegQueryValueEx() 
{
	m_bRegQueryValueEx = !m_bRegQueryValueEx;
	UpdateHookFunction();
}

void CRegisterMonView::OnUpdateRegQueryValueEx(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bRegQueryValueEx);	
}


void CRegisterMonView::UpdateHookFunction()
{
	if(m_Moudle)
	{
		typedef void (*SetHookFunction)(BOOL, BOOL, BOOL, BOOL, BOOL);
		SetHookFunction pFunc = NULL;
		pFunc = (SetHookFunction)::GetProcAddress(m_Moudle, "SetHookFunction");
		if(pFunc)
			(*pFunc)(m_bRegCreateKeyEx, m_bRegSetValueEx, m_bRegDeleteKey, m_bRegDeleteValue, m_bRegQueryValueEx);
	}

}

void CRegisterMonView::OnFilterExplorer() 
{
	DWORD pid = 0;
	if(!m_bIgnoreExplorer)
		pid = GetProcessID("explorer.exe");

	if(m_Moudle)
	{
		typedef void (*SetExplorerPID)(DWORD);
		SetExplorerPID pFunc = NULL;
		pFunc = (SetExplorerPID)::GetProcAddress(m_Moudle, "SetExplorerPID");
		if(pFunc)
		{
			(*pFunc)(pid);
		}
	}
	m_bIgnoreExplorer = !m_bIgnoreExplorer;
}

void CRegisterMonView::OnUpdateFilterExplorer(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_hHook != NULL);
	pCmdUI->SetCheck(m_bIgnoreExplorer);
}

void CRegisterMonView::OnFilterRegedit() 
{
	m_bIgnoreRegedit = !m_bIgnoreRegedit;
}

void CRegisterMonView::OnUpdateFilterRegedit(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_hHook != NULL);
	pCmdUI->SetCheck(m_bIgnoreRegedit);
}

void CRegisterMonView::OnFileSave() 
{
	CFileDialog dlg(FALSE, "html", "", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "网页文件(*.html)|*.html|CSV(逗号分隔)(*.csv)|*.csv|");
	if(IDOK == dlg.DoModal())
	{
		DeleteFile(dlg.GetFileName());
		CListCtrl& theCtrl = GetListCtrl();
		int nCount = theCtrl.GetItemCount();
		CFile file(dlg.GetFileName(), CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite);
		CString s;
		if (dlg.GetFileExt() == "html")
		{
			s = "<table width=\"1200\" height=\"100\" border=1 align=\"center\" bodercolor=#ffaa00>"
			"<tr><th>进程名</th><th>行为</th><th>键名</th><th>名称</th><th>数据</th><th>类型</th>";
			for(int i=0; i<nCount; i++)
			{
				s += "<tr><th>" +
					theCtrl.GetItemText(i, 0) + "</th><th>" +
					theCtrl.GetItemText(i, 1) + "</th><th>" +
					theCtrl.GetItemText(i, 2) + "</th><th>" +
					theCtrl.GetItemText(i, 3) + "</th><th>" +
					theCtrl.GetItemText(i, 4) + "</th><th>" +
					theCtrl.GetItemText(i, 5) + "</th>";
			}
			s += "</table>";
			file.SeekToEnd();
			file.Write(s, s.GetLength());
			file.Close();
			MessageBox("导出html文件完成！", "友情提示", MB_OK | MB_ICONINFORMATION);
		}
		else
		{
			s = "进程名,行为,键名,名称,数据,类型\r\n";
			for(int i=0; i<nCount; i++)
			{
				s += theCtrl.GetItemText(i, 0) + "," +
					theCtrl.GetItemText(i, 1) + "," +
					theCtrl.GetItemText(i, 2) + "," +
					theCtrl.GetItemText(i, 3) + "," +
					theCtrl.GetItemText(i, 4) + "," +
					theCtrl.GetItemText(i, 5) + "\r\n";
			}
			file.SeekToEnd();
			file.Write(s, s.GetLength());
			file.Close();
			MessageBox("导出csv文件完成！", "友情提示", MB_OK | MB_ICONINFORMATION);
		}
	}
}

void CRegisterMonView::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	CListCtrl& theCtrl = GetListCtrl();
	pCmdUI->Enable(theCtrl.GetItemCount());
}

void CRegisterMonView::OnFileDelete() 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nIndex = theCtrl.GetNextSelectedItem(pos);
		theCtrl.DeleteItem(nIndex);
		theCtrl.SetItemState(nIndex, LVIS_SELECTED, LVIS_SELECTED); 
	}
}

void CRegisterMonView::OnUpdateFileDelete(CCmdUI* pCmdUI) 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	pCmdUI->Enable(pos>0);
}

void CRegisterMonView::OnViewApp() 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nItem = theCtrl.GetNextSelectedItem(pos);
		char szText[300];
		sprintf(szText, "/select, %s", theCtrl.GetItemText(nItem, 0));
		ShellExecute(m_hWnd, "Open", "Explorer.exe", szText, NULL, 1);
	}
}

void CRegisterMonView::OnUpdateViewApp(CCmdUI* pCmdUI) 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	pCmdUI->Enable(pos>0);
}

void CRegisterMonView::OnViewReg() 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nItem = theCtrl.GetNextSelectedItem(pos);
		CString s = theCtrl.GetItemText(nItem, 2);
		HKEY hKey;
		DWORD dwDisposition;   
		DWORD size = s.GetLength();
		long ret = RegCreateKeyEx(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit",
			0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);
		ret = RegSetValueEx(hKey, "LastKey", NULL, REG_SZ, (const BYTE*)s.GetBuffer(0), size);
		RegCloseKey(hKey);
		ShellExecute(m_hWnd, "Open", "regedit.exe", NULL, NULL, 1);
	}
}

void CRegisterMonView::OnUpdateViewReg(CCmdUI* pCmdUI) 
{
	CListCtrl& theCtrl = GetListCtrl();
	POSITION pos = theCtrl.GetFirstSelectedItemPosition();
	pCmdUI->Enable(pos>0);
}


void CRegisterMonView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_DELETE)
		OnFileDelete();
	if (nChar == VK_RETURN)
		OnLButtonDblClk(NULL, NULL);
	CListView::OnKeyDown(nChar, nRepCnt, nFlags);
}
