// RegisterMonDoc.h : interface of the CRegisterMonDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_REGISTERMONDOC_H__850C98A6_D146_4676_8357_A992A0EFF4AA__INCLUDED_)
#define AFX_REGISTERMONDOC_H__850C98A6_D146_4676_8357_A992A0EFF4AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CRegisterMonDoc : public CDocument
{
protected: // create from serialization only
	CRegisterMonDoc();
	DECLARE_DYNCREATE(CRegisterMonDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegisterMonDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRegisterMonDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRegisterMonDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGISTERMONDOC_H__850C98A6_D146_4676_8357_A992A0EFF4AA__INCLUDED_)
