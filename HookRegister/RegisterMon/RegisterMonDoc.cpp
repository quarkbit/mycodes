// RegisterMonDoc.cpp : implementation of the CRegisterMonDoc class
//

#include "stdafx.h"
#include "RegisterMon.h"

#include "RegisterMonDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonDoc

IMPLEMENT_DYNCREATE(CRegisterMonDoc, CDocument)

BEGIN_MESSAGE_MAP(CRegisterMonDoc, CDocument)
	//{{AFX_MSG_MAP(CRegisterMonDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonDoc construction/destruction

CRegisterMonDoc::CRegisterMonDoc()
{
	// TODO: add one-time construction code here

}

CRegisterMonDoc::~CRegisterMonDoc()
{
}

BOOL CRegisterMonDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CRegisterMonDoc serialization

void CRegisterMonDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonDoc diagnostics

#ifdef _DEBUG
void CRegisterMonDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRegisterMonDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRegisterMonDoc commands
