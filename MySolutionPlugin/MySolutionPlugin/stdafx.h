// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef STRICT
#define STRICT
#endif

#include "targetver.h"

#define _ATL_APARTMENT_THREADED

#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit


#define ATL_NO_ASSERT_ON_DESTROY_NONEXISTENT_WINDOW

#include "resource.h"
#include <atlbase.h>
#include <atlcom.h>
#include <atlctl.h>

#define ZY_DEBUG
#ifdef ZY_DEBUG
// ZY, Macros for debug
#include <atlbase.h>
#include <atlstr.h>

#define DEBUG_INSERT_BREAK if (IsDebuggerPresent()) {__asm{INT 3};}
#define DEBUG_SHOW_MESSAGEBOX do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	ATL::CString debugStr; \
	debugStr.Format(_T("ZYDEBUG, %S, Line: %d\nProcessID: %d[0x%x]\nThreadID: %d[0x%x]\n"), __FUNCDNAME__, __LINE__, proID, proID, threadID, threadID); \
	::MessageBox(NULL, debugStr, _T("For Debug"), 0); \
} while(0)

#define DEBUG_OUTPUT_MESSAGE do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	ATL::CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, %s, %d\n"), proID, threadID, __FUNCTIONW__, __LINE__); \
	::OutputDebugString(debugStr); \
} while(0)

#define DEBUG_OUTPUT(FORMAT_STR, ...) do \
{ \
	DWORD proID = ::GetCurrentProcessId(); \
	DWORD threadID = ::GetCurrentThreadId(); \
	ATL::CString debugStr; \
	debugStr.Format(_T("[%d, %d] ZYDEBUG, "), proID, threadID); \
	debugStr.AppendFormat(FORMAT_STR, __VA_ARGS__); \
	::OutputDebugString(debugStr); \
} while(0)
#else
#define DEBUG_INSERT_BREAK 
#define DEBUG_SHOW_MESSAGEBOX 
#define DEBUG_OUTPUT_MESSAGE 
#define DEBUG_OUTPUT
#endif

