#ifndef H_BHOFRAMEWND_H_0410d130_097e_4375_bf17_8fef093f405f
#define H_BHOFRAMEWND_H_0410d130_097e_4375_bf17_8fef093f405f
#include <atlwin.h>
using namespace ATL;


class CMyToolbarWnd  : public CWindowImpl<CMyToolbarWnd>
{
public:
	CMyToolbarWnd(void);
	~CMyToolbarWnd(void);
public:
	// 窗口类名
	DECLARE_WND_CLASS(_T("CMyToolbarWnd")) 
	//消息映射
	BEGIN_MSG_MAP(CMyToolbarWnd)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)  
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE,OnMouseMove)
		MESSAGE_HANDLER(WM_MOUSELEAVE, OnMouseLeave)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBKGnd)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLBtnDown)
		MESSAGE_HANDLER(WM_LBUTTONUP,   OnLBtnUp)
		MESSAGE_HANDLER(WM_RBUTTONDOWN,   OnLBtnDown)
		MESSAGE_HANDLER(WM_RBUTTONUP,   OnLBtnUp)
	END_MSG_MAP()
public:
	LRESULT OnCreate(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnPaint(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnDestroy(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnMouseMove(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnMouseLeave(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnEraseBKGnd(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnSize(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnLBtnDown(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
	LRESULT OnLBtnUp(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled);
public:
	CComPtr<IHTMLElement> m_spFlashPlayer;
private:
};
#endif//H_BHOFRAMEWND_H_0410d130_097e_4375_bf17_8fef093f405f

