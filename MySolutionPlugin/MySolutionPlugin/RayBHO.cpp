// RayBHO.cpp : Implementation of CRayBHO

#include "stdafx.h"
#include "RayBHO.h"
#include <shlguid.h>
#include <assert.h>
#include "EventHandler.h"

// Global variables used in this module
static HHOOK g_hHook=NULL; // Handle for the keyboard hook
static CRayBHO *g_pThis; // Pointer to this class

// CRayBHO

STDMETHODIMP CRayBHO::SetSite( IUnknown* pUnkSite )
{
	//DEBUG_SHOW_MESSAGEBOX;
	if (pUnkSite)
	{

		// Retrieve and store the IWebBrowser2 pointer
		m_spWebBrowser2 = pUnkSite;
		if (m_spWebBrowser2 == NULL)
			return E_INVALIDARG;

		// Retrieve and store the IConnectionPointerContainer pointer
		m_spCPC = m_spWebBrowser2;
		if (m_spCPC == NULL)
			return E_POINTER;

		// Retrieve and store the HWND of the browser. Plus install
		// a keyboard hook
		// RetrieveBrowserWindow();

		// Connect to the container for receiving event notifications
		return Connect();
	}
	else
	{
		return Disconnect();
	}
}

void CRayBHO::OnDocumentComplete( IDispatch* pDisp, BSTR URL )
{
	CComPtr<IDispatch> pCurDisp;
	m_spWebBrowser2->QueryInterface(IID_IDispatch, (void**)&pCurDisp);
	if (pCurDisp != pDisp)
	{
		return;
	}

	DEBUG_OUTPUT(_T("OnDocumentComplete: %s"), URL);

	InitToolbarByDoc();

}

HRESULT CRayBHO::HandlePlugins(IHTMLDocument2* pDocument)
{
	HRESULT hr = S_OK;

	CComPtr<IHTMLElement> spBody;
	hr = pDocument->get_body(&spBody);
	if (FAILED(hr) || !spBody)
	{
		return hr;
	}
	
	CComPtr<IHTMLElement2> spBody2;
	hr = spBody->QueryInterface(IID_IHTMLElement2, (void**)&spBody2);
	if (FAILED(hr) || !spBody2)
	{
		return hr;
	}
	

	CComBSTR spObjStr = L"OBJECT";
	CComPtr<IHTMLElementCollection> spElems;
	hr = spBody2->getElementsByTagName(spObjStr, &spElems);
	if (SUCCEEDED(hr) && spElems)
	{
		long elemCount = 0;
		hr = spElems->get_length(&elemCount);
		DEBUG_OUTPUT(_T("element count = %d"), elemCount);
		if (SUCCEEDED(hr) && elemCount > 0)
		{
			int maxWidth = 0;
			int maxHeight = 0;
			int maxLeft = 0;
			int maxTop = 0;

			for (int i=0; i<elemCount; i++)
			{
				CComVariant svarItemIndex(i);
				CComVariant svarEmpty;
				CComPtr<IDispatch> spdispElem;
				hr = spElems->item(svarItemIndex, svarEmpty, &spdispElem);

				if (SUCCEEDED(hr) && spdispElem)
				{
					CComQIPtr<IHTMLElement> spElement = spdispElem;
					if (spElement)
					{
						//CComBSTR outerHtml;
						//spElement->get_outerHTML(&outerHtml);
						//DEBUG_OUTPUT(_T("Element: %p\t %s"), spElement, outerHtml); 

						CComPtr<IDispatch> spFlashDisp;
						hr = spElement->QueryInterface(IID_IShockwaveFlash, (void**)&spFlashDisp);
						if (FAILED(hr))
						{
							continue;
						}

						long width;
						long height;
						long left = 0;
						long top = 0;

						spElement->get_offsetWidth(&width);
						spElement->get_offsetHeight(&height);

						if (width < 100 || height < 100)
						{
							continue;
						}

						CComPtr<IHTMLElement2> spElement2;
						hr = spElement->QueryInterface(IID_IHTMLElement2, (void**)&spElement2);

						if (SUCCEEDED(hr) && spElement2)
						{
							IHTMLRect* boundRect;
							spElement2->getBoundingClientRect(&boundRect);
							boundRect->get_left(&left);
							boundRect->get_top(&top);

							if (width * height > maxHeight * maxWidth)
							{
								maxWidth = width;
								maxHeight = height;
								maxLeft = left;
								maxTop = top;
								m_spFlashPlayer = spElement;
							}

							CComPtr<IHTMLObjectElement> spObj;
							spElement->QueryInterface(IID_IHTMLObjectElement, (void**)&spObj);
							assert(spObj);

							
							VARIANT eh = CEventHandler::createEventHandlerVariant(CRayBHO::OnDocumentClickStub, this);
							spObj->put_onreadystatechange(eh);

							DEBUG_OUTPUT(_T("Index:%d\t\t bounding[%4d, %4d, %4d, %4d]"), 
								i, left, top, width, height);
						}
					}
				}
			}

			if (maxHeight * maxWidth > 0)
			{
				CreateToolbar();
				::MoveWindow(m_pToolbar->m_hWnd, maxLeft, maxTop - 25, 125, 25, TRUE);
			}

		}
		else
		{
			hr = E_UNEXPECTED;
		}
	}
	return hr;
}

void CRayBHO::RemoveImages(IHTMLDocument2* pDocument)
{
	CComPtr<IHTMLElementCollection> spImages;
	// 从 DOM 中获取图像集。
	HRESULT hr = pDocument->get_images(&spImages);
	if (hr == S_OK && spImages != NULL) {
		// 获取集合中的图像数。
		long cImages = 0;
		hr = spImages->get_length(&cImages);
		if (hr == S_OK && cImages > 0)
		{
			for (int i = 0; i < cImages; i++)
			{
				CComVariant svarItemIndex(i);
				CComVariant svarEmpty;
				CComPtr<IDispatch> spdispImage;
				// 按索引从集合中获取图像。
				hr = spImages->item(svarItemIndex, svarEmpty, &spdispImage);
				if (hr == S_OK && spdispImage != NULL)
				{
					// 首先，查询通用 HTML 元素接口……
					CComQIPtr<IHTMLElement> spElement = spdispImage;
					if (spElement)
					{
						// ……然后请求样式接口。
						CComPtr<IHTMLStyle> spStyle;
						hr = spElement->get_style(&spStyle);
						// 设置 display="none" 以隐藏图像。
						if (hr == S_OK && spStyle != NULL)
						{
							static const CComBSTR sbstrNone(L"none");
							spStyle->put_display(sbstrNone);
						}
					}
				}
			}
		}
	}
}

void CRayBHO::OnBeforeNavigate2( IDispatch* pDisp, BSTR pUrl )
{
	CComPtr<IDispatch> pCurDisp;
	m_spWebBrowser2->QueryInterface(IID_IDispatch, (void**)&pCurDisp);
	if (pCurDisp != pDisp)
	{
		return;
	}

	m_bInited = FALSE;
	DEBUG_OUTPUT(_T("OnBeforeNavigate2: %s"), pUrl);
}

HRESULT CRayBHO::GetIEPageHandles( IDispatch* pDisp )
{
	HRESULT hr = ERROR_SUCCESS;
	CComPtr<IServiceProvider> sp;
	hr = pDisp->QueryInterface(&sp);

	if (SUCCEEDED(hr) && sp)
	{
		// Get Tab Window Handle
		CComPtr<IOleWindow> spow;
		sp->QueryService( SID_SShellBrowser, IID_IOleWindow, (void**)&spow );
		if( SUCCEEDED(hr) && spow)
		{
			HWND hWnd = NULL;
			hr = spow->GetWindow( &hWnd );
			if( SUCCEEDED(hr) && hWnd )
			{
				m_hTabWindow = hWnd;

				HWND hDocView = ::FindWindowEx(m_hTabWindow, NULL, _T("Shell DocObject View"), NULL);

				m_hIEServer = ::FindWindowEx(hDocView, NULL, _T("Internet Explorer_Server"), NULL);
				DEBUG_OUTPUT(_T("hIEServer, %p"), m_hIEServer);
			}
		}

	}

	return hr;
}

void CRayBHO::DestroyToolbar()
{
	if (m_pToolbar)   
	{
		if (m_pToolbar->m_hWnd)
		{
			m_pToolbar->DestroyWindow();
		}
		delete m_pToolbar;
		m_pToolbar = NULL;
	}
}

void CRayBHO::ManipulateDOM( IHTMLDocument2* pDocument )
{
	HRESULT hr;
	CComPtr<IHTMLElementCollection> spElems;
	hr = pDocument->get_all(&spElems);
	if (SUCCEEDED(hr) && spElems)
	{
		long elemCount = 0;
		hr = spElems->get_length(&elemCount);
		DEBUG_OUTPUT(_T("element count = %d"), elemCount);
		if (SUCCEEDED(hr) && elemCount > 0)
		{
			for (int i=0; i<elemCount; i++)
			{
				CComVariant svarItemIndex(i);
				CComVariant svarEmpty;
				CComPtr<IDispatch> spdispElem;
				hr = spElems->item(svarItemIndex, svarEmpty, &spdispElem);

				if (SUCCEEDED(hr) && spdispElem)
				{
					CComPtr<IHTMLElement> spElement;
					spdispElem->QueryInterface(IID_IHTMLElement, (void**)&spElement);
			
					CComBSTR spTagName;
					spElement->get_tagName(&spTagName);
					CComBSTR spID;
					spElement->get_id(&spID);

					long width, height, left, top;

					spElement->get_offsetWidth(&width);
					spElement->get_offsetHeight(&height);
					spElement->get_offsetLeft(&left);
					spElement->get_offsetTop(&top);
			

					DEBUG_OUTPUT(_T("%s,%s [%d, %d, %d, %d]"), spTagName, spID, left, top, width, height);

					if (spID && wcscmp(spID, L"Text1") == 0)
					{
						CComVariant spVar;
						hr = spElement->get_onclick(&spVar);
						spElement->click();

						CreateToolbar();
						::MoveWindow(m_pToolbar->m_hWnd, left - 1, top - 1, width + 2, height + 2, TRUE);

					}

			

				}
			}
		}
	}
}

void CRayBHO::CreateToolbar()
{
	if (!m_pToolbar)
	{
		m_pToolbar = new CMyToolbarWnd();
		HWND hWnd = m_pToolbar->Create(m_hIEServer, NULL, _T("CMyToolbarWindow"), 
			WS_CHILD | WS_VISIBLE, WS_EX_TOPMOST | WS_EX_TOOLWINDOW);
		m_pToolbar->m_spFlashPlayer = m_spFlashPlayer;
	}
}

void CRayBHO::OnDownloadBegin()
{
}

void CRayBHO::OnDownloadComplete()
{
}

void CRayBHO::InitToolbarByDoc()
{
	HRESULT hr = ERROR_SUCCESS;
	CComPtr<IDispatch>  spDispDoc;
	hr = m_spWebBrowser2->get_Document(&spDispDoc);
	if (SUCCEEDED(hr))
	{
		CComQIPtr<IHTMLDocument2> spHTMLDoc = spDispDoc;

		if (spHTMLDoc != NULL) 
		{
			GetIEPageHandles(m_spWebBrowser2);

			VARIANT eventHandler = CEventHandler::createEventHandlerVariant(CRayBHO::OnWindowScrolledStub, this);
			CComQIPtr<IHTMLWindow2> spTempWindow2;
			hr = spHTMLDoc->get_parentWindow(&spTempWindow2);

			spTempWindow2->put_onscroll(eventHandler);
			spTempWindow2->put_onresize(eventHandler);

			hr = HandlePlugins(spHTMLDoc);
			if (SUCCEEDED(hr))
			{
				DEBUG_OUTPUT(_T("Toolbar Init SUCCEEDED!"));
				m_bInited = TRUE;
			}
		}
	}
}

STDMETHODIMP CRayBHO::Disconnect( void )
{
	HRESULT hr;
	CComPtr<IConnectionPoint> spCP;

	// Receives the connection point for WebBrowser events
	hr = m_spCPC->FindConnectionPoint(
		DIID_DWebBrowserEvents,
		&spCP);
	if (FAILED(hr))
		return hr;

	// Stop getting event notifications
	hr = spCP->Unadvise(m_dwCookie);
	m_dwCookie = 0;
	return hr;
} 

STDMETHODIMP CRayBHO::Connect( void )
{
	HRESULT hr;
	CComPtr<IConnectionPoint> spCP;

	// Receives the connection point for WebBrowser events
	hr = m_spCPC->FindConnectionPoint(
		DIID_DWebBrowserEvents2,
		&spCP);
	if (FAILED(hr))
		return hr;

	// Pass the event handlers to the container
	hr = spCP->Advise(
		reinterpret_cast<IDispatch*>(this),
		&m_dwCookie);

	return hr;
}

STDMETHODIMP CRayBHO::RetrieveBrowserWindow( void )
{

	// To get the HWND of the browser we need to enumerate
	// all the windows belonging to this thread. We could
	// check for the window's class name if it wasn't changed
	// from Windows 95/NT 4.0 to Windows 98. With Windows 98
	// the class name is the same as any other open folder, so
	// there's no certainty of uniqueness.

	// Get the HWND of the browser's window
	EnumThreadWindows(
		GetCurrentThreadId(),
		WndEnumProc,
		reinterpret_cast<LPARAM>(&m_hwndBrowser) );

	if (!IsWindow(m_hwndBrowser))
		return E_POINTER;

	// We need a way to toggle the view state of the Code
	// Window. By the means of a local hook we'll be able
	// to handle keys before they are sent to the IE wndproc

	// Set the keyboard hook
	g_hHook = SetWindowsHookEx( WH_KEYBOARD,
		reinterpret_cast<HOOKPROC>(IEKeyboardProc),
		NULL,
		GetCurrentThreadId() );

	// Store pointer to this class. This global pointer will
	// be used by the hook procedure which is a static member
	// of the class and couldn't access otherwise its members
	g_pThis = this;
	return S_OK;
}

STDMETHODIMP CRayBHO::OnQuit( void )
{
	return Disconnect();
}

STDMETHODIMP CRayBHO::Invoke( DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pDispParams, VARIANT* pvarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr )
{
	if( !pDispParams ) {
		return E_INVALIDARG;
	}

	HRESULT hr = S_OK;

	switch(dispidMember)
	{
	case DISPID_BEFORENAVIGATE2:
		{
			assert(pDispParams->cArgs == 7);
			IDispatch* pDisp = pDispParams->rgvarg[6].pdispVal;
			BSTR pUrl = pDispParams->rgvarg[5].pvarVal->bstrVal;
			OnBeforeNavigate2(pDisp, pUrl);
		}
		break;
	case DISPID_DOCUMENTCOMPLETE:
		{
			assert(pDispParams->cArgs == 2);
			IDispatch* pDisp = pDispParams->rgvarg[1].pdispVal;
			BSTR pUrl = pDispParams->rgvarg[0].pvarVal->bstrVal;
			OnDocumentComplete(pDisp, pUrl);
		}
		break;
	case DISPID_NAVIGATECOMPLETE2:
		{
			DEBUG_OUTPUT(_T("DISPID_NAVIGATECOMPLETE2: %d, Var Count: %d"), dispidMember, pDispParams->cArgs);
		}
		break;
	case DISPID_DOWNLOADBEGIN:
		{
			DEBUG_OUTPUT(_T("DISPID_DOWNLOADBEGIN: %d, Var Count: %d"), dispidMember, pDispParams->cArgs);
		}
		break;
	case DISPID_DOWNLOADCOMPLETE:
		{
			DEBUG_OUTPUT(_T("DISPID_DOWNLOADCOMPLETE: %d, Var Count: %d"), dispidMember, pDispParams->cArgs);
		}
		break;
	case DISPID_NEWWINDOW:
	case DISPID_NEWWINDOW2:
		{
			DEBUG_OUTPUT(_T("DISPID_NEWWINDOW: %d, Var Count: %d"), dispidMember, pDispParams->cArgs);
		}
		break;
	case DISPID_WINDOWRESIZE:
		{
			DEBUG_OUTPUT(_T("DISPID_WINDOWRESIZE: %d, Var Count: %d"), dispidMember, pDispParams->cArgs);
		}
		break;
	case DISPID_STATUSTEXTCHANGE:
	case DISPID_COMMANDSTATECHANGE:
		{
			//UpdateToolbarPosition();
		}
		break;
	case DISPID_ONQUIT:
		OnQuit();
		break;
	default:
		{
			DEBUG_OUTPUT(_T("DispidMember: %d, Var Count: %d"), dispidMember, pDispParams->cArgs);
		}
		break;
	}

	return hr;
}

BOOL CALLBACK CRayBHO::WndEnumProc( HWND hwnd, LPARAM lParam )
{
	TCHAR szClassName[MAX_PATH];
	GetClassName(hwnd, szClassName, MAX_PATH);

	// IEFrame was the window's class name under Win95. The
	// other is the title under Win98.
	if (!lstrcmpi(szClassName, _T("CabinetWClass")) ||
		!lstrcmpi(szClassName, _T("IEFrame")))
	{
		// We need to return the HWND found if any. The lParam
		// is a pointer to the HWND to be used the return buffer
		HWND *phWnd = reinterpret_cast<HWND*>(lParam);
		*phWnd = hwnd;

		// Return FALSE to stop enumeration once we've found the
		// expected window
		return false;
	}

	// Return TRUE to continue enumerating windows
	return true;
}

LRESULT CALLBACK CRayBHO::IEKeyboardProc( int nCode, WPARAM wParam, LPARAM lParam )
{
	// Typical start-off for any hook
	if( nCode <0 )
		return CallNextHookEx(g_hHook, nCode, wParam, lParam);

	// Process the key only once
	if ((lParam & 0x80000000) || (lParam & 0x40000000))
		return CallNextHookEx(g_hHook, nCode, wParam, lParam);

	// The key we're interested in is F11. If it's been pressed
	// then forces the Code Window to be refreshed
	if (wParam==VK_F11)
	{
		// MessageBox(NULL, "KEY F11 is pressed!", "IENameHandler", MB_OK);
	}

	// Typical end for any hook procedure
	return CallNextHookEx(g_hHook, nCode, wParam, lParam);
}

void CRayBHO::UpdateToolbarPosition()
{
	if (m_spFlashPlayer)
	{
		CComPtr<IHTMLElement2> spElement2;
		HRESULT hr;
		hr = m_spFlashPlayer->QueryInterface(IID_IHTMLElement2, (void**)&spElement2);

		if (SUCCEEDED(hr) && spElement2)
		{
			long left, top;
			IHTMLRect* boundRect;
			spElement2->getBoundingClientRect(&boundRect);
			boundRect->get_left(&left);
			boundRect->get_top(&top);

			if (m_pToolbar->IsWindow())
			{
				m_pToolbar->MoveWindow(left, top - 25, 125, 25, TRUE);
			}
		}
	}
}

void CRayBHO::OnDocumentClickStub( VARIANT* varResult, void* cookie )
{
	CRayBHO* actualObject = static_cast<CRayBHO*>(cookie);
	assert(actualObject!=NULL);

	actualObject->OnDocumentClick(varResult);
}

void CRayBHO::OnDocumentClick( VARIANT* varResult )
{
	DEBUG_OUTPUT_MESSAGE;
}

void CRayBHO::OnWindowScrolled( VARIANT* varResult )
{
	DEBUG_OUTPUT_MESSAGE;
	UpdateToolbarPosition();
}

void CRayBHO::OnWindowScrolledStub( VARIANT* varResult, void* cookie )
{
	CRayBHO* actualObject = static_cast<CRayBHO*>(cookie);
	assert(actualObject!=NULL);

	actualObject->OnWindowScrolled(varResult);
}
