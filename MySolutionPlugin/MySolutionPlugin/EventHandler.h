#pragma once

typedef void (*eventCallbackFunctionPtr_t)(VARIANT* pVarResult, void* cookie);

// Private implementation class to handle the COM glue needed for element event callbacks
class CEventHandler : public IDispatch 
{
public:
	CEventHandler(eventCallbackFunctionPtr_t callback, void* cookie); 
	~CEventHandler();

	HRESULT __stdcall QueryInterface(REFIID riid, void** ppvObject);
	DWORD __stdcall AddRef();
	DWORD __stdcall Release();

	STDMETHOD(GetTypeInfoCount)(unsigned int FAR* pctinfo) { return E_NOTIMPL; }
	STDMETHOD(GetTypeInfo)(unsigned int iTInfo, LCID  lcid, ITypeInfo FAR* FAR*  ppTInfo) { return E_NOTIMPL; }

	STDMETHOD(GetIDsOfNames)(REFIID riid, OLECHAR FAR* FAR* rgszNames, unsigned int cNames, LCID lcid, DISPID FAR* rgDispId) { return S_OK; }

	STDMETHOD(Invoke)(DISPID dispIdMember,
		REFIID riid,
		LCID lcid,
		WORD wFlags,
		DISPPARAMS* pDispParams,
		VARIANT* pVarResult,
		EXCEPINFO * pExcepInfo, 
		UINT * puArgErr);

	static LPDISPATCH createEventHandler(eventCallbackFunctionPtr_t callbackFunction, void* cookie);
	static VARIANT createEventHandlerVariant(eventCallbackFunctionPtr_t callbackFunction, void* cookie);

protected:

	eventCallbackFunctionPtr_t _callbackFunction;
	void* _cookie;
	long _refCount;
};