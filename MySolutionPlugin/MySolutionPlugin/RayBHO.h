// RayBHO.h : Declaration of the CRayBHO

#pragma once
#include "resource.h"   // main symbols
//#include <exdispid.h>
//#include <ShlGuid.h>



#include "MySolutionPlugin_i.h"
#include <exdispid.h>
#include "MyToolbarWnd.h"
#include <mshtmdid.h>
#include <mshtml.h>



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
	const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

MIDL_DEFINE_GUID(IID, IID_IShockwaveFlash,0xD27CDB6C,0xAE6D,0x11CF,0x96,0xB8,0x44,0x45,0x53,0x54,0x00,0x00);

// CRayBHO

class ATL_NO_VTABLE CRayBHO :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRayBHO, &CLSID_RayBHO>,
	public IObjectWithSiteImpl<CRayBHO>,
	public IDispatchImpl<IRayBHO, &IID_IRayBHO, &LIBID_MySolutionPluginLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CRayBHO() :
		m_hTabWindow(NULL),
		m_hIEServer(NULL),
		m_fAdvised(FALSE),
		m_pToolbar(NULL),
		m_bInited(FALSE)
	{
	}

	~CRayBHO()
	{
		DestroyToolbar();
	}

DECLARE_REGISTRY_RESOURCEID(IDR_RAYBHO)

DECLARE_NOT_AGGREGATABLE(CRayBHO)

BEGIN_COM_MAP(CRayBHO)
	COM_INTERFACE_ENTRY(IRayBHO)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	STDMETHOD(SetSite)(IUnknown* pUnkSite);

	// IDispatch
public:
	STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pDispParams, VARIANT* pvarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr);

	void UpdateToolbarPosition();

private:
	STDMETHOD(Connect)(void);
	STDMETHOD(Disconnect)(void);
	STDMETHOD(RetrieveBrowserWindow)(void);
	STDMETHOD(OnQuit)(void);
	static BOOL CALLBACK WndEnumProc(HWND hwnd, LPARAM lParam);
	static LRESULT CALLBACK IEKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);

private:
	void OnDocumentComplete(IDispatch* pDisp, BSTR URL); 
	void OnBeforeNavigate2 ( IDispatch* pDisp, BSTR pUrl );
	void OnDownloadBegin();
	void OnDownloadComplete();

	void OnScroll (IHTMLEventObj *pEvtObj);
	void ConnectEvents(IHTMLElement* pElem);

	static void OnDocumentClickStub(VARIANT* varResult, void* cookie);
	void OnDocumentClick(VARIANT* varResult);

	static void OnWindowScrolledStub(VARIANT* varResult, void* cookie);
	void OnWindowScrolled(VARIANT* varResult);

	void InitToolbarByDoc();
	void RemoveImages(IHTMLDocument2* pDocument);
	HRESULT HandlePlugins(IHTMLDocument2* pDocument);
	void CreateToolbar();
	void ManipulateDOM(IHTMLDocument2* pDocument);
	HRESULT GetIEPageHandles(IDispatch* pDisp);
	void DestroyToolbar();
private:
	//CComPtr<IWebBrowser2> m_spWebBrowser2;
	HWND m_hTabWindow;
	HWND m_hIEServer;
	BOOL m_fAdvised;
	CMyToolbarWnd* m_pToolbar;
	BOOL m_bInited;

	CComPtr<IHTMLElement> m_spFlashPlayer;
	CComQIPtr<IWebBrowser2, &IID_IWebBrowser2> m_spWebBrowser2;
	CComQIPtr<IConnectionPointContainer, &IID_IConnectionPointContainer> m_spCPC;
	DWORD m_dwCookie;
	HWND m_hwndBrowser;
};


OBJECT_ENTRY_AUTO(__uuidof(RayBHO), CRayBHO)
