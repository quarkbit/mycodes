// dllmain.h : Declaration of module class.

class CMySolutionPluginModule : public ATL::CAtlDllModuleT< CMySolutionPluginModule >
{
public :
	DECLARE_LIBID(LIBID_MySolutionPluginLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_MYSOLUTIONPLUGIN, "{4CEC1912-977B-4E89-93D5-4C656075B506}")
};

extern class CMySolutionPluginModule _AtlModule;
