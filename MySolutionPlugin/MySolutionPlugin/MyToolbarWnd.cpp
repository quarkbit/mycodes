#include "stdafx.h"
#include "MyToolbarWnd.h"




CMyToolbarWnd::CMyToolbarWnd(void)
{

}

CMyToolbarWnd::~CMyToolbarWnd(void)
{

}

VOID CALLBACK MY_TIMERPROC(HWND hWnd,UINT nMsg,UINT nTimerid,DWORD dwTime)
{
}

LRESULT CMyToolbarWnd::OnCreate(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{
	DEBUG_OUTPUT_MESSAGE;
	//::SetTimer(m_hWnd, 0, 50, MY_TIMERPROC);
	return S_OK;
}



LRESULT CMyToolbarWnd::OnDestroy(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{
	//::KillTimer(m_hWnd, 0);
	return S_OK;
}

LRESULT CMyToolbarWnd::OnEraseBKGnd(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{
	return S_FALSE;
}

LRESULT CMyToolbarWnd::OnSize(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{

	return S_OK;
}

LRESULT CMyToolbarWnd::OnPaint(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{
	RECT loClientRect = {0};
	::GetClientRect(m_hWnd, &loClientRect);

	
	PAINTSTRUCT ps;
	BeginPaint(&ps);

	HDC hdc = GetDC();

	HPEN pen = CreatePen(PS_DASH, 1, RGB(255, 0, 0));
	SelectObject(hdc, pen);

	HBRUSH brush = CreateSolidBrush(RGB(70, 227, 236));
	SelectObject(hdc, brush);

	//::FillRect(hdc, &loClientRect, brush);
	::Rectangle(hdc, 0, 0, loClientRect.right, loClientRect.bottom);
	
	DeleteObject(brush);
	DeleteObject(pen);
	DeleteDC(hdc);

	EndPaint(&ps);
	return S_OK;
}



LRESULT CMyToolbarWnd::OnLBtnDown(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{

	return S_OK;
}

LRESULT CMyToolbarWnd::OnLBtnUp(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{
	DEBUG_SHOW_MESSAGEBOX;
	//if (m_spFlashPlayer)
	//{
	//	HRESULT hr;
	//	CComPtr<IDispatch> spDispDoc;
	//	m_spFlashPlayer->get_document(&spDispDoc);
	//	CComPtr<IHTMLDocument2> spDoc;
	//	spDispDoc->QueryInterface(IID_IHTMLDocument2, (void**)&spDoc);
	//	CComPtr<IHTMLElement> spBody;
	//	spDoc->get_body(&spBody);

	//	CComPtr<IHTMLElement2> spElement2;
	//	spBody->QueryInterface(IID_IHTMLElement2, (void**)&spElement2);
	//	VARIANT comp;
	//	comp.vt = VT_BSTR;
	//	comp.bstrVal = L"scrollbarPageDown";
	//}
	return S_OK;
}

LRESULT CMyToolbarWnd::OnMouseMove(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{
	
	return S_OK;
}

LRESULT CMyToolbarWnd::OnMouseLeave(UINT aMsg, WPARAM awParam, LPARAM alParam, BOOL& abHandled)
{

	return S_OK;
}
