#include "stdafx.h"
#include "EventHandler.h"

// --------------------------
// Event handler helper class
// --------------------------

CEventHandler::CEventHandler(eventCallbackFunctionPtr_t callback, void* cookie)
{
	_callbackFunction = callback;
	_cookie = cookie;
	_refCount = 0;
}

CEventHandler::~CEventHandler()
{
}

HRESULT __stdcall CEventHandler::QueryInterface(REFIID riid, void** ppvObject)
{
	*ppvObject = NULL;

	if (IsEqualGUID(riid, IID_IUnknown))
		*ppvObject = reinterpret_cast<void**>(this);

	if (IsEqualGUID(riid, IID_IDispatch))
		*ppvObject = reinterpret_cast<void**>(this);

	if (*ppvObject)
	{
		((IUnknown*)*ppvObject)->AddRef();
		return S_OK;
	}
	else
	{
		return E_NOINTERFACE;
	}
}

DWORD __stdcall CEventHandler::AddRef()
{
	return InterlockedIncrement(&_refCount);
}

DWORD __stdcall CEventHandler::Release()
{
	if (InterlockedDecrement(&_refCount) == 0)
	{
		delete this;
		return 0;
	}
	return _refCount;
}

HRESULT CEventHandler::Invoke(DISPID dispIdMember,
							  REFIID riid,
							  LCID lcid,
							  WORD wFlags,
							  DISPPARAMS* pDispParams,
							  VARIANT* pVarResult,
							  EXCEPINFO * pExcepInfo, 
							  UINT * puArgErr)
{
	if (dispIdMember == DISPID_VALUE)
		(*_callbackFunction)(pVarResult, _cookie);

	return S_OK;
}

LPDISPATCH CEventHandler::createEventHandler(eventCallbackFunctionPtr_t callbackFunction, void* cookie)
{
	CEventHandler* newHandler = new CEventHandler(callbackFunction, cookie);
	return reinterpret_cast<LPDISPATCH>(newHandler);
}

VARIANT CEventHandler::createEventHandlerVariant(eventCallbackFunctionPtr_t callbackFunction, void* cookie)
{
	VARIANT variant;
	variant.vt = VT_DISPATCH;
	variant.pdispVal = CEventHandler::createEventHandler(callbackFunction, cookie);
	return variant;
}
